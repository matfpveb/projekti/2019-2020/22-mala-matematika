const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const studentsAPIRoutes = require('./components/student/studentsAPI');
const teachersAPIRoutes = require('./components/teacher/teachersAPI');
const parentsAPIRoutes = require('./components/parent/parentsAPI');
const classroomsAPIRoutes = require('./components/classroom/classroomsAPI');
const fieldsAPIRoutes = require('./components/field/fieldsAPI');
const topicsAPIRoutes = require('./components/topic/topicsAPI');
const tasksAPIRoutes = require('./components/task/tasksAPI');
const profilesAPIRoutes = require('./components/profile/profileAPI');
const statisticAPIRoutes = require('./components/statistic/statisticAPI');

// Kreiranje Express.js aplikacije
const app = express();

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

// Konekcija na MongoDB SUBP
mongoose.connect('mongodb://127.0.0.1:27017/MalaMatematika', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
});

// Parsiranje tela zahteva za dva formata:
// 1) application/x-www-form-urlencoded
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
// 2) application/json
app.use(bodyParser.json({}));

// Implementacija CORS zastite
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');

  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
    return res.status(200).json({});
  }

  next();
});

// Definisanje osnovnih pravila za rutiranje
app.use('/students', studentsAPIRoutes);
app.use('/teachers', teachersAPIRoutes);
app.use('/parents', parentsAPIRoutes);
app.use('/classrooms', classroomsAPIRoutes);
app.use('/fields', fieldsAPIRoutes);
app.use('/topics', topicsAPIRoutes);
app.use('/tasks', tasksAPIRoutes);
app.use('/profiles', profilesAPIRoutes);
app.use('/statistics', statisticAPIRoutes);

// Obrada zahteva koji se ne poklapa sa nekom pravilom od iznad
app.use(function (req, res, next) {
  const error = new Error('Zahtev nije podrzan od servera');
  error.status = 405;

  next(error);
});

// Obrada svih gresaka u nasoj aplikaciji
app.use(function (error, req, res, next) {
    const statusCode = error.status || 500;
  res.status(statusCode).json({
    error: {
      message: error.message,
      status: statusCode,
      stack: error.stack
    },
  });
});

// Izvozenje Express.js aplikacije radi pokretanja servera
module.exports = app;
