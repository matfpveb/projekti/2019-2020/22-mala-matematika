# MalaMatematika

## Development server

Run `nodemon .\server.js` or `node .\server.js` to start a server. It's started on `http://localhost:3000/`.

## Database 

Run `mongod` to start MongoDB

### Populating DB with fields and topics

Run shell on your computer and go into 22-mala-matematika\mala-matematika-server\intro_db folder. 
You need to run two scripts. 
First run `mongo < script_deleting.js`
Then run `mongo < script_populating.js`