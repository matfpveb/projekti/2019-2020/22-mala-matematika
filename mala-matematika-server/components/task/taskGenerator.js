const math = require('./mathFunctions');
const e = require('express');

const text_easy = "Izračunaj i odaberi tačan odgovor";
const text_medium_and_hard = "Izračunaj i unesi tačan odgovor";

module.exports.addingDoubleDigitAndOneDigit = function(level) {
    if (level == 'easy' || level == 'medium') {
        const doubleDigit = math.getRandomIntInclusive(10,99);

        // Da nemamo prenos pri sabiranju korigujemo opseg za drugi sabirak
        const secondDigitOfDoubleDigit = doubleDigit % 10;
        const oneDigit = math.getRandomIntInclusive(1,9-secondDigitOfDoubleDigit);

        // text_easy = "Izračunaj i odaberi tačan odgovor"
        // text_medium = "Izračunaj i unesi tačan odgovor"
        
        task = doubleDigit.toString() + " + " + oneDigit.toString() + " = ";
        solution = doubleDigit + oneDigit;

        // Za medium nivo ne nudimo potencijalna resenja
        if(level == 'medium') {
            return JSON.stringify({ 
                text: text_medium_and_hard, 
                task: task,
                solution: solution
            });
        }
        
        // Za easy nivo nudimo potencijalna resenja
        // Another way for generating possibilities:
        // solution + math.getRandomIntInclusive(1,5), solution - math.getRandomIntInclusive(1,5)
        possibleSolutions = math.shuffle([solution, solution + oneDigit, doubleDigit - oneDigit])
        return JSON.stringify({ 
            text: text_easy, 
            task: task,
            solution: solution,
            possibleSolutions: possibleSolutions
        });
    } else if (level == 'hard') {
        const doubleDigit = math.getRandomIntInclusive(10,99);

        const oneDigit = doubleDigit > 90 ? math.getRandomIntInclusive(1,9-(doubleDigit%10)) : math.getRandomIntInclusive(1,9);

        // text = "Izračunaj i unesi tačan odgovor"
        task = doubleDigit.toString() + " + " + oneDigit.toString() + " = ";
        solution = doubleDigit + oneDigit;

        return JSON.stringify({ 
            text: text_medium_and_hard, 
            task: task,
            solution: solution
        });
    }
};

module.exports.substractingOneDigitFromDoubleDigit = function(level) {
    if (level == 'easy' || level == 'medium') {

        // Oduzimamo jednocifren od dvocifrenog broja
        const doubleDigit = math.getRandomIntInclusive(10,99);
        const singleDigit = math.getRandomIntInclusive(1, (doubleDigit%10));

        task = doubleDigit.toString() + " - " + singleDigit.toString() + " = ";
        solution = doubleDigit - singleDigit;

        // Za medium nivo ne nudimo potencijalna resenja
        if(level == 'medium') {
            return JSON.stringify({ 
                text: text_medium_and_hard, 
                task: task,
                solution: solution
            });
        }

        tmpFalseSolution = solution + math.getRandomIntInclusive(1, 9);
        firstFalseSolution = (tmpFalseSolution > 100) ? 99 : tmpFalseSolution;

        tmpFalseSolution = solution - math.getRandomIntInclusive(1, 9);
        secondFalseSolution = (tmpFalseSolution < 1) ? 1 : tmpFalseSolution

        possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
        return JSON.stringify({ 
            text: text_easy, 
            task: task,
            solution: solution,
            possibleSolutions: possibleSolutions
        });


    } else if (level == 'hard') { 
        const doubleDigit = math.getRandomIntInclusive(10,99);
        const singleDigit = math.getRandomIntInclusive(1, 9);

        // text = "Izračunaj i unesi tačan odgovor"
        task = doubleDigit.toString() + " - " + singleDigit.toString() + " = ";
        solution = doubleDigit - singleDigit;

        return JSON.stringify({ 
            text: text_medium_and_hard, 
            task: task,
            solution: solution
        });
    }
};

module.exports.addingDoubleDigits = function(level) {
    if (level == 'easy' || level == 'medium') {

        // Sabiramo 2 dvocifrena broja, ciji zbir ne sme da predje 99
        const firstDoubleDigit = math.getRandomIntInclusive(10,89);
        tmpSecondDigit = math.getRandomIntInclusive(10, 99-firstDoubleDigit);

        // Odredjujemo poslednju cifru drugog broja da ne bi doslo do prenosa prilikomsabiranja
        tmpSecondDigit -= (tmpSecondDigit%10);
        const lastDigitOfSecondNumber = math.getRandomIntInclusive(0, 9-(firstDoubleDigit%10));
        const secondDoubleDigit = tmpSecondDigit + lastDigitOfSecondNumber;

        task = firstDoubleDigit.toString() + " + " + secondDoubleDigit.toString() + " = ";
        solution = firstDoubleDigit + secondDoubleDigit;

        // Za medium nivo ne nudimo potencijalna resenja
        if(level == 'medium') {
            return JSON.stringify({ 
                text: text_medium_and_hard, 
                task: task,
                solution: solution
            });
        }

        tmpFalseSolution = solution + math.getRandomIntInclusive(1, 9);
        firstFalseSolution = (tmpFalseSolution > 100) ? 99 : tmpFalseSolution;

        tmpFalseSolution = solution - math.getRandomIntInclusive(1, 9);
        secondFalseSolution = (tmpFalseSolution < 1) ? 1 : tmpFalseSolution

        possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
        return JSON.stringify({ 
            text: text_easy, 
            task: task,
            solution: solution,
            possibleSolutions: possibleSolutions
        });


    } else if (level == 'hard') { 
        const firstDoubleDigit = math.getRandomIntInclusive(10,89);
        const secondDoubleDigit = math.getRandomIntInclusive(10, 99-firstDoubleDigit);

        // text = "Izračunaj i unesi tačan odgovor"
        task = firstDoubleDigit.toString() + " + " + secondDoubleDigit.toString() + " = ";
        solution = firstDoubleDigit + secondDoubleDigit;

        return JSON.stringify({ 
            text: text_medium_and_hard, 
            task: task,
            solution: solution
        });
    }
};

module.exports.substractingDoubleDigitFromDoubleDigit = function(level) {
    if (level == 'easy' || level == 'medium') {

        // Oduzimmo 2 dvocifrena broja
        const firstDoubleDigit = math.getRandomIntInclusive(10,99);
        tmpSecondDigit = math.getRandomIntInclusive(10, firstDoubleDigit);

        // Odredjujemo poslednju cifru drugog broja da ne bi doslo do pozajmice prilikom oduzimanja
        tmpSecondDigit -= (tmpSecondDigit%10);
        const lastDigitOfSecondNumber = math.getRandomIntInclusive(0, (firstDoubleDigit%10));
        const secondDoubleDigit = tmpSecondDigit + lastDigitOfSecondNumber;

        task = firstDoubleDigit.toString() + " - " + secondDoubleDigit.toString() + " = ";
        solution = firstDoubleDigit - secondDoubleDigit;

        // Za medium nivo ne nudimo potencijalna resenja
        if(level == 'medium') {
            return JSON.stringify({ 
                text: text_medium_and_hard, 
                task: task,
                solution: solution
            });
        }

        tmpFalseSolution = solution + math.getRandomIntInclusive(1, 9);
        firstFalseSolution = (tmpFalseSolution > 100) ? 99 : tmpFalseSolution;

        tmpFalseSolution = solution - math.getRandomIntInclusive(1, 9);
        secondFalseSolution = (tmpFalseSolution < 0) ? 0 : tmpFalseSolution

        possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
        return JSON.stringify({ 
            text: text_easy, 
            task: task,
            solution: solution,
            possibleSolutions: possibleSolutions
        });


    } else if (level == 'hard') { 
        const firstDoubleDigit = math.getRandomIntInclusive(10,99);
        const secondDoubleDigit = math.getRandomIntInclusive(10, firstDoubleDigit);

        // text = "Izračunaj i unesi tačan odgovor"
        task = firstDoubleDigit.toString() + " - " + secondDoubleDigit.toString() + " = ";
        solution = firstDoubleDigit - secondDoubleDigit;

        return JSON.stringify({ 
            text: text_medium_and_hard, 
            task: task,
            solution: solution
        });
    }
};

module.exports.changePlacesOfAdditives = function(level) {

    // Drugi sabirak mora biti veci od prvog, da bi imalo smisla da im menjamo mesta
    const firstAdditive = math.getRandomIntInclusive(1,49);
    tmpSecondAdditive = math.getRandomIntInclusive(firstAdditive + 1, 99);
    if (firstAdditive + tmpSecondAdditive > 99) {
        minSubstraction = firstAdditive + tmpSecondAdditive - 99;
        maxSubstraction = tmpSecondAdditive - 50;
        tmpSecondAdditive -= math.getRandomIntInclusive(minSubstraction, maxSubstraction);
    }

    const secondAdditive = tmpSecondAdditive;
    const solution = secondAdditive + "+" + firstAdditive;
    const sum = firstAdditive + secondAdditive;
    const task = firstAdditive.toString() + " + " + secondAdditive.toString() + " = ";

    if (level == 'easy') {
        firstFalseSolution = firstAdditive + "+" + secondAdditive;
        secondFalseSolution = secondAdditive + "-" + firstAdditive;
        possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
        text = "Odaberi odgovor u kojem su mesta datih sabiraka zamenjena pravilno";
        return JSON.stringify({ 
            text: text, 
            task: task,
            solution: solution,
            possibleSolutions: possibleSolutions
        });
    } else if (level == 'medium') {
        text = "Unesi odgovor u kojem su mesta datih sabiraka zamenjena pravilno";
        return JSON.stringify({ 
            text: text, 
            task: task,
            solution: solution
        });
    } else if (level == 'hard') {
        text = "Unesi odgovor u kojem su mesta datih sabiraka zamenjena pravilno, a zatim izracunaj njihov zbir";
        return JSON.stringify({ 
            text: text, 
            task: task,
            solution: solution,
            sum: sum
        });
    }
};

module.exports.addingWithSumOrDifference = function(level) {
    // 1 - first brackets, sum
    // 2 - second brackets, sum
    // 3 - first bracket, diff
    // 4 - second brackets, diff
    randomNumber = math.getRandomIntInclusive(1, 4);

    switch (randomNumber) {
        case 1:
            firstNumber = math.getRandomIntInclusive(1, 97);
            secondNumber = math.getRandomIntInclusive(1, 98 - firstNumber);
            thirdNumber = math.getRandomIntInclusive(1, 99 - (firstNumber+secondNumber));
            task = "(" + firstNumber.toString() + " + " + secondNumber.toString() + ") + " + thirdNumber.toString() + " = ";

            firstPartSolution = firstNumber + secondNumber
            solution = firstPartSolution + thirdNumber;
            firstFalseSolution = solution > 99 ? solution - 1 : solution + 1;
            secondFalseSolution = solution < 2 ? solution + 1 : solution - 1;

            break;
        case 2:
            firstNumber = math.getRandomIntInclusive(1, 97);
            secondNumber = math.getRandomIntInclusive(1, 98 - firstNumber)
            thirdNumber = math.getRandomIntInclusive(1, 99 - (firstNumber+secondNumber));
            task = firstNumber.toString() + " + " + "(" + secondNumber.toString() + " + " + thirdNumber.toString() + ")" + " = ";

            firstPartSolution = secondNumber + thirdNumber;
            solution = firstNumber + firstPartSolution;
            firstFalseSolution = solution > 99 ? solution - 1 : solution + 1;
            secondFalseSolution = solution < 2 ? solution + 1 : solution - 1;

            break;
        case 3:
            firstNumber = math.getRandomIntInclusive(2, 99);
            secondNumber = math.getRandomIntInclusive(1, firstNumber);
            thirdNumber = math.getRandomIntInclusive(1, 99 - (firstNumber-secondNumber))
            task = "(" + firstNumber.toString() + " - " + secondNumber.toString() + ") +" + thirdNumber.toString() + " = ";

            firstPartSolution = firstNumber - secondNumber;
            solution = firstPartSolution + thirdNumber;
            firstFalseSolution = solution > 99 ? solution - 1 : solution + 1;
            secondFalseSolution = solution < 2 ? solution + 1 : solution - 1;

            break;

        case 4:
            firstNumber = math.getRandomIntInclusive(2, 99);
            secondNumber = math.getRandomIntInclusive(1, firstNumber);
            thirdNumber = math.getRandomIntInclusive(1, 99 - (firstNumber-secondNumber))
            task = thirdNumber.toString() + " + " + "(" + firstNumber.toString() + " - " + secondNumber.toString() + ")" + " = ";

            firstPartSolution = firstNumber - secondNumber;
            solution = firstPartSolution + thirdNumber;
            firstFalseSolution = solution > 99 ? solution - 1 : solution + 1;
            secondFalseSolution = solution < 2 ? solution + 1 : solution - 1;

            break;
        
    }

    switch(level) {
        case "easy":   
            possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
            return JSON.stringify({ 
                text: text_easy, 
                task: task,
                solution: solution,
                possibleSolutions: possibleSolutions,
                randomNumber: randomNumber
            });
        case "medium":
            return JSON.stringify({ 
                text: text_medium_and_hard,
                task: task,
                solution: solution,
                firstSolution: firstPartSolution,
                randomNumber: randomNumber
            });
        case "hard":
            return JSON.stringify({ 
                text: text_medium_and_hard,
                task: task,
                solution: solution,
                randomNumber: randomNumber
            });
    }
};

module.exports.addingAndSubstractingFromZero = function(level) {

    // 1 - num + 0
    // 2 - 0 + num
    // 3 - num - 0
    // 4 - num - num
    randomNumber = math.getRandomIntInclusive(1, 4);
    
    firstNumber = math.getRandomIntInclusive(1, 99);
    secondNumber = 0

    switch(randomNumber) {
        case 1:
            if(level == "hard")
            task = "_ + 0 = " + firstNumber.toString();
            else
                task = firstNumber.toString() + " + 0 = ";
            solution = firstNumber; 
            firstFalseSolution = (firstNumber == 99) ? 98 : firstNumber + 1;
            secondFalseSolution = (firstNumber == 1) ? 2 : firstNumber - 1;
            break;

        case 2:
            if(level == "hard")
                task = "0 + _ = " + firstNumber.toString();
            else
                task = "0 + " + firstNumber.toString() + " = ";
            solution = firstNumber; 
            firstFalseSolution = (firstNumber == 99) ? 98 : firstNumber + 1;
            secondFalseSolution = (firstNumber == 1) ? 2 : firstNumber - 1;
            break; 

        case 3:
            if(level == "hard")
                task = "_ - 0 = " + firstNumber.toString();
            else
                task = firstNumber.toString() + " - 0 = ";
            solution = firstNumber;
            firstFalseSolution = (firstNumber == 99) ? 98 : firstNumber + 1;
            secondFalseSolution = (firstNumber == 1) ? 2 : firstNumber - 1;
            break;

        case 4:
            if(level == "hard")
                task = firstNumber.toString() + " - _ = " + firstNumber.toString();
            else
                task = firstNumber.toString() + " - " + firstNumber.toString() + " = ";
            solution = 0;
            firstFalseSolution = 1;
            secondFalseSolution = 2;
            break;
    }


    if (level == 'easy') {
        possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
        return JSON.stringify({ 
            text: text_easy,
            task: task,
            solution: solution,
            possibleSolutions: possibleSolutions
        });
    } else if (level == 'medium') {
        return JSON.stringify({ 
            text: text_medium_and_hard, 
            task: task,
            solution: solution
        });
    } else if (level == 'hard') {
        return JSON.stringify({ 
            text: text_medium_and_hard, 
            task: task,
            solution: solution
        });
    }
};

module.exports.substractingSumOrDifferenceFromNumber = function(level) {

    if(level == "hard") {
        firstNumber = math.getRandomIntInclusive(1, 99);
        secondNumber = math.getRandomIntInclusive(1, 99);
        thirdNumber = (firstNumber > secondNumber) ? math.getRandomIntInclusive(1, secondNumber) : 
                                                     math.getRandomIntInclusive(secondNumber - firstNumber, secondNumber);
                
        task = firstNumber.toString() + " - " + "(" + secondNumber.toString() + " - " + thirdNumber.toString() + ") = ";
        solution = firstNumber - secondNumber + thirdNumber;
        return JSON.stringify({ 
            text: text_medium_and_hard, 
            task: task,
            solution: solution
        });
    }
    else {
        firstNumber = math.getRandomIntInclusive(1, 99);
        secondNumber = math.getRandomIntInclusive(0, firstNumber-1);
        thirdNumber = math.getRandomIntInclusive(0, firstNumber - secondNumber - 1);
        task = firstNumber.toString() + " - " + "(" + secondNumber.toString() + " + " + thirdNumber.toString() + ") = ";
        solution = firstNumber - secondNumber - thirdNumber;

        if(level == "easy") {
            firstFalseSolution = (solution < 3) ? 3 : solution - 1;
            secondFalseSolution = (solution > 98) ? 98 : solution + 1;
            
            possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
            return JSON.stringify({ 
                text: text_easy,
                task: task,
                solution: solution,
                possibleSolutions: possibleSolutions
            });
        }
        else {
            return JSON.stringify({ 
                text: text_medium_and_hard, 
                task: task,
                solution: solution
            });
        }
    }
};

module.exports.substractingNumberFromSumOrDifference = function(level) {

    if(level == "hard") {
        firstNumber = math.getRandomIntInclusive(1, 99);
        secondNumber = math.getRandomIntInclusive(1, firstNumber - 1);
        thirdNumber = math.getRandomIntInclusive(1, firstNumber - secondNumber);
                
        task = "(" + firstNumber.toString() + " - " + secondNumber.toString() + ") - " + thirdNumber.toString() + " = ";
        solution = firstNumber - secondNumber - thirdNumber;
        return JSON.stringify({ 
            text: text_medium_and_hard, 
            task: task,
            solution: solution
        });
    }
    else {
        firstNumber = math.getRandomIntInclusive(1, 99);
        secondNumber = math.getRandomIntInclusive(0, 99-firstNumber-1);
        thirdNumber = math.getRandomIntInclusive(0, firstNumber + secondNumber - 1);
        task = "(" + firstNumber.toString() + " + " + secondNumber.toString() + ") - " + thirdNumber.toString() + " = ";
        solution = firstNumber + secondNumber - thirdNumber;

        if(level == "easy") {
            firstFalseSolution = (solution < 3) ? 3 : solution - 1;
            secondFalseSolution = (solution > 98) ? 98 : solution + 1;
            
            possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
            return JSON.stringify({ 
                text: text_easy,
                task: task,
                solution: solution,
                possibleSolutions: possibleSolutions
            });
        }
        else {
            return JSON.stringify({ 
                text: text_medium_and_hard, 
                task: task,
                solution: solution
            });
        }
    }
};

module.exports.determiningUnknownAdditive = function(level) {

    firstNumber = math.getRandomIntInclusive(0, 99);
    sumOfNumbers = math.getRandomIntInclusive(firstNumber, 99);
    solution = sumOfNumbers - firstNumber;

    if(level == "hard") {
        task = "x + " + firstNumber.toString() + " = " + sumOfNumbers.toString() + ", x = ";
        return JSON.stringify({ 
            text: text_medium_and_hard, 
            task: task,
            solution: solution
        });
    }
    else {
        task = firstNumber.toString() + " + x = " + sumOfNumbers.toString() + ", x = ";

        if(level == "easy") {
            firstFalseSolution = (solution < 2) ? 2 : solution - 1;
            secondFalseSolution = (solution > 98) ? 98 : solution + 1;
            
            possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
            return JSON.stringify({ 
                text: text_easy,
                task: task,
                solution: solution,
                possibleSolutions: possibleSolutions
            });
        }
        else {
            return JSON.stringify({ 
                text: text_medium_and_hard, 
                task: task,
                solution: solution
            });
        }
    }
};

module.exports.determiningUnknownMinuend = function(level) {

    firstNumber = math.getRandomIntInclusive(0, 49);
    diffOfNumbers = math.getRandomIntInclusive(0, 49-firstNumber);
    solution = diffOfNumbers + firstNumber;

    if(level == "hard") {
        firstNumber = math.getRandomIntInclusive(49, 99);
        diffOfNumbers = math.getRandomIntInclusive(49, 99-firstNumber);
        solution = diffOfNumbers + firstNumber;
        task = "x - " + firstNumber.toString() + " = " + diffOfNumbers.toString() + ", x = ";
        return JSON.stringify({ 
            text: text_medium_and_hard, 
            task: task,
            solution: solution
        });
    }
    else {
        task = "x - " + firstNumber.toString() + " = " + diffOfNumbers.toString() + ", x = ";

        if(level == "easy") {
            firstFalseSolution = (solution < 2) ? 2 : solution - 1;
            secondFalseSolution = (solution > 98) ? 98 : solution + 1;
            
            possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
            return JSON.stringify({ 
                text: text_easy,
                task: task,
                solution: solution,
                possibleSolutions: possibleSolutions
            });
        }
        else {
            return JSON.stringify({ 
                text: text_medium_and_hard, 
                task: task,
                solution: solution
            });
        }
    }
};

module.exports.determiningUnknownSubtrahend = function(level) {

    firstNumber = math.getRandomIntInclusive(0, 49);
    diffOfNumbers = math.getRandomIntInclusive(0, firstNumber);
    solution =  firstNumber - diffOfNumbers;

    if(level == "hard") {
        firstNumber = math.getRandomIntInclusive(49, 99);
        diffOfNumbers = math.getRandomIntInclusive(49, firstNumber);
        solution = firstNumber - diffOfNumbers;
        task = firstNumber.toString() + " - x = " + diffOfNumbers.toString() + ", x = ";
        return JSON.stringify({ 
            text: text_medium_and_hard, 
            task: task,
            solution: solution
        });
    }
    else {
        task = firstNumber.toString() + " - x = " + diffOfNumbers.toString() + ", x = ";

        if(level == "easy") {
            firstFalseSolution = (solution < 2) ? 2 : solution - 1;
            secondFalseSolution = (solution > 98) ? 98 : solution + 1;
            
            possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
            return JSON.stringify({ 
                text: text_easy,
                task: task,
                solution: solution,
                possibleSolutions: possibleSolutions
            });
        }
        else {
            return JSON.stringify({ 
                text: text_medium_and_hard, 
                task: task,
                solution: solution
            });
        }
    }
};

module.exports.multiplyByNumberAndThatNumber = function(level, number) {
    if(level == "medium" || level == "hard") {
        firstNumber = math.getRandomIntInclusive(5, 10);
    } else if (level == "easy") {
        firstNumber = math.getRandomIntInclusive(0, 4);
    }

    solution = firstNumber * number;

    // Posto imamo mnozenje nekim brojem (number) i mnozenje tog broja nekim
    if (Math.random() < 0.5) {
        task = firstNumber.toString() + " * " + number.toString() + " = ";
    } else {
        task = number.toString() + " * " + firstNumber.toString() + " = ";
    }

    if(level == "easy") {
        firstFalseSolution = solution + number;
        secondFalseSolution = firstNumber + number;
            
        possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
        return JSON.stringify({ 
            text: text_easy,
            task: task,
            solution: solution,
            possibleSolutions: possibleSolutions
        });
    }
        
    return JSON.stringify({ 
        text: text_medium_and_hard, 
        task: task,
        solution: solution
    });
};

module.exports.multiplyingNumber2AndBy2 = function(level) {
    return this.multiplyByNumberAndThatNumber(level, 2);
};

module.exports.multiplyingNumber3AndBy3 = function(level) {
    return this.multiplyByNumberAndThatNumber(level, 3);
};

module.exports.multiplyingNumber4AndBy4 = function(level) {
    return this.multiplyByNumberAndThatNumber(level, 4);
};

module.exports.multiplyingNumber5AndBy5 = function(level) {
    return this.multiplyByNumberAndThatNumber(level, 5);
};

module.exports.multiplyingNumber6AndBy6 = function(level) {
    return this.multiplyByNumberAndThatNumber(level, 6);
};

module.exports.multiplyingNumber7AndBy7 = function(level) {
    return this.multiplyByNumberAndThatNumber(level, 7);
};

module.exports.multiplyingNumber8AndBy8 = function(level) {
    return this.multiplyByNumberAndThatNumber(level, 8);
};

module.exports.multiplyingNumber9AndBy9 = function(level) {
    return this.multiplyByNumberAndThatNumber(level, 9);
};

module.exports.multiplyingNumber10AndBy10 = function(level) {
    return this.multiplyByNumberAndThatNumber(level, 10);
};

module.exports.multiplyingWithZeroAndOne = function(level) {
    number = math.getRandomIntInclusive(0, 1);

    if(level == "medium" || level == "hard") {
        firstNumber = math.getRandomIntInclusive(5, 10);
    } else if(level == "easy") {
        firstNumber = math.getRandomIntInclusive(0, 4);
    }  
    
    solution = firstNumber * number;

    // Posto imamo mnozenje nekim brojem (number) i mnozenje tog broja nekim
    if (Math.random() < 0.5) {
        task = firstNumber.toString() + " * " + number.toString() + " = ";
    } else {
        task = number.toString() + " * " + firstNumber.toString() + " = ";
    }
       
    if(level == "easy") {
        if(number == 0) {
            firstFalseSolution = firstNumber;
            secondFalseSolution = 1;
        } else {
            firstFalseSolution = 1;
            secondFalseSolution = firstNumber + number;
        }

        possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
        return JSON.stringify({ 
            text: text_easy,
            task: task,
            solution: solution,
            possibleSolutions: possibleSolutions
        });
    }
        
    return JSON.stringify({ 
        text: text_medium_and_hard, 
        task: task,
        solution: solution
    });
};

module.exports.multiplyTwoNumbers = function(level) {
    switch(math.getRandomIntInclusive(1, 10)) {
        case 1:
            return this.multiplyingWithZeroAndOne(level);
        case 2:
            return this.multiplyingNumber2AndBy2(level);
        case 3:
            return this.multiplyingNumber3AndBy3(level);
        case 4:
            return this.multiplyingNumber4AndBy4(level);    
        case 5:
            return this.multiplyingNumber5AndBy5(level);  
        case 6:
            return this.multiplyingNumber6AndBy6(level);  
        case 7:
            return this.multiplyingNumber7AndBy7(level); 
        case 8:
            return this.multiplyingNumber8AndBy8(level);  
        case 9:
            return this.multiplyingNumber9AndBy9(level);  
        case 10:
            return this.multiplyingNumber10AndBy10(level);
        default:
            {};
    }
};

module.exports.multiplyingSumWithOneDigitNumber = function(level) {
    firstNumber = math.getRandomIntInclusive(1, 9); // The number multiplying with
    valid_sum_limit = 100 / firstNumber;
    
    if(level == "easy") {
        secondNumber = math.getRandomIntInclusive(1, 10);
        thirdNumber = math.getRandomIntInclusive(1, valid_sum_limit - secondNumber);
    } else if (level == "medium") { // Two digit plus one digit or reverse in sum
        secondNumber = math.getRandomIntInclusive(0, valid_sum_limit);
        thirdNumber = math.getRandomIntInclusive(0, valid_sum_limit - secondNumber);
    } else if(level == "hard") { // Two digit numbers in sum
        secondNumber = math.getRandomIntInclusive(10, valid_sum_limit);
        thirdNumber = math.getRandomIntInclusive(10, valid_sum_limit - secondNumber);
    }

    solution = firstNumber * (secondNumber + thirdNumber);

    // Split representation on either multiplying number by sum or reverse
    if (Math.random() < 0.5) {
        task = firstNumber.toString() + " * (" + secondNumber.toString() + " + " + thirdNumber.toString() + ")" + " = ";
    } else {
        task = "(" + secondNumber.toString() + " + " + thirdNumber.toString() + ") * " + firstNumber.toString() + " = ";
    }

    if(level == "easy") {
        firstFalseSolution = firstNumber * secondNumber + thirdNumber;
        secondFalseSolution = firstNumber * thirdNumber + secondNumber;
        
        possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
        return JSON.stringify({ 
            text: text_easy,
            task: task,
            solution: solution,
            possibleSolutions: possibleSolutions
        });
    }
   
    return JSON.stringify({ 
        text: text_medium_and_hard, 
        task: task,
        solution: solution
    });
};

module.exports.multiplyingDifferenceWithOneDigitNumber = function(level) {
    firstNumber = math.getRandomIntInclusive(1, 9); // The number multiplying with
    valid_sum_limit = 100 / firstNumber;
    
    if(level == "easy") {
        secondNumber = math.getRandomIntInclusive(1, valid_sum_limit);
        thirdNumber = math.getRandomIntInclusive(1, secondNumber);
    } else if(level == "medium" || level == "hard") { 
        secondNumber = math.getRandomIntInclusive(10, valid_sum_limit);
        thirdNumber = math.getRandomIntInclusive(1, secondNumber);
    }

    solution = firstNumber * (secondNumber - thirdNumber);

    // Split representation on either multiplying number by sum or reverse
    if (Math.random() < 0.5) {
        task = firstNumber.toString() + " * (" + secondNumber.toString() + " - " + thirdNumber.toString() + ")" + " = ";
    } else {
        task = "(" + secondNumber.toString() + " - " + thirdNumber.toString() + ") * " + firstNumber.toString() + " = ";
    }

    if(level == "easy") {
        firstFalseSolution = firstNumber * secondNumber - thirdNumber;
        secondFalseSolution = secondNumber - thirdNumber*firstNumber;
        
        possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
        return JSON.stringify({ 
            text: text_easy,
            task: task,
            solution: solution,
            possibleSolutions: possibleSolutions
        });
    }
   
    return JSON.stringify({ 
        text: text_medium_and_hard, 
        task: task,
        solution: solution
    });
};

module.exports.multiplyingSumOrDifferenceWithOneDigitNumber = function(level) {
    switch(math.getRandomIntInclusive(1, 2)) {
        case 1:
            return this.multiplyingSumWithOneDigitNumber(level);
        case 2:
            return this.multiplyingDifferenceWithOneDigitNumber(level);
        default:
            {};
    }
};

module.exports.multiplyingOneDigitByTwoDigit = function(level) {
    if (level == "easy") {
        firstNumber = math.getRandomIntInclusive(5, 9);
    } else if(level == "medium" || level == "hard") {
        firstNumber = math.getRandomIntInclusive(0, 4);
    }

    valid_sum_limit = 100 / firstNumber;
    secondNumber = math.getRandomIntInclusive(10, valid_sum_limit);
    solution = firstNumber * secondNumber;
    
    // Posto imamo mnozenje nekim brojem (number) i mnozenje tog broja nekim
    if (Math.random() < 0.5) {
        task = firstNumber.toString() + " * " + secondNumber.toString() + " = ";
    } else {
        task = secondNumber.toString() + " * " + firstNumber.toString() + " = ";
    }

    if(level == "easy") {
        firstFalseSolution = firstNumber + secondNumber;
        secondFalseSolution = firstNumber - secondNumber;
        
        possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
        return JSON.stringify({ 
            text: text_easy,
            task: task,
            solution: solution,
            possibleSolutions: possibleSolutions
        });
    }
       
    return JSON.stringify({ 
        text: text_medium_and_hard, 
        task: task,
        solution: solution
    });
};

module.exports.divisionByNumber = function(level, number) {

    if(level == "hard") {
        firstNumber = math.getRandomIntInclusive(5, 10) * number;
        solution = firstNumber / number;
        task = firstNumber.toString() + " : " + number.toString()  + " = ";
        return JSON.stringify({ 
            text: text_medium_and_hard, 
            task: task,
            solution: solution
        });
    }
    else {
        firstNumber = math.getRandomIntInclusive(0, 4) * number;
        solution =  firstNumber / number;
        task = firstNumber.toString() + " : " + number.toString()  + " = ";
        if(level == "easy") {
            firstFalseSolution = (solution < 2) ? 2 : solution - 1;
            secondFalseSolution = (solution > number) ? number : solution + 1;
            
            possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
            return JSON.stringify({ 
                text: text_easy,
                task: task,
                solution: solution,
                possibleSolutions: possibleSolutions
            });
        }
        else {
            return JSON.stringify({ 
                text: text_medium_and_hard, 
                task: task,
                solution: solution
            });
        }
    }
};

module.exports.divisionBy2 = function(level) {
    return this.divisionByNumber(level, 2);
};

module.exports.divisionBy3 = function(level) {
    return this.divisionByNumber(level, 3);
};

module.exports.divisionBy4 = function(level) {
    return this.divisionByNumber(level, 4);
};

module.exports.divisionBy5 = function(level) {
    return this.divisionByNumber(level, 5);
};

module.exports.divisionBy6 = function(level) {
    return this.divisionByNumber(level, 6);
};

module.exports.divisionBy7 = function(level) {
    return this.divisionByNumber(level, 7);
};

module.exports.divisionBy8 = function(level) {
    return this.divisionByNumber(level, 8);
};

module.exports.divisionBy9 = function(level) {
    return this.divisionByNumber(level, 9);
};

module.exports.divisionBy10 = function(level) {
    return this.divisionByNumber(level, 10);
};

module.exports.division0WithNumber = function(level) {
    if(level == "hard") {
        firstNumber = math.getRandomIntInclusive(6, 10);
        solution = 0;
        task = "0 : " + firstNumber.toString() + " = ";
        return JSON.stringify({ 
            text: text_medium_and_hard, 
            task: task,
            solution: solution
        });
    }
    else {
        firstNumber = math.getRandomIntInclusive(1, 5);
        solution = 0;
        task = "0 : " + firstNumber.toString() + " = ";
        if(level == "easy") {
            firstFalseSolution = 1;
            secondFalseSolution = 2;
            
            possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
            return JSON.stringify({ 
                text: text_easy,
                task: task,
                solution: solution,
                possibleSolutions: possibleSolutions
            });
        }
        else {
            return JSON.stringify({ 
                text: text_medium_and_hard, 
                task: task,
                solution: solution
            });
        }
    }
};

module.exports.divisionBy1 = function(level) {
    return this.divisionByNumber(level, 1);
};

module.exports.divisionOfSumWithOneDigitNumber = function(level) {
    thirdNumber = math.getRandomIntInclusive(1, 9); // The number dividing with

    if(level == "easy") { //TODO razmisliti da li moze drugacije, ovako npr nece biti generisano (17+3)/2
        firstNumber = math.getRandomIntInclusive(1, 4) * thirdNumber;
        secondNumber = math.getRandomIntInclusive(1, 4) * thirdNumber;
    } else if (level == "medium") { 
        firstNumber = math.getRandomIntInclusive(1, 4) * thirdNumber;
        secondNumber = math.getRandomIntInclusive(5, 10) * thirdNumber;
    } else if(level == "hard") { 
        firstNumber = math.getRandomIntInclusive(5, 10) * thirdNumber;
        secondNumber = math.getRandomIntInclusive(5, 10) * thirdNumber;
    }

    solution = (firstNumber + secondNumber) / thirdNumber;

    task = "(" + firstNumber.toString() + " + " + secondNumber.toString() + ") : " + thirdNumber.toString() + " = ";

    if(level == "easy") {
        firstFalseSolution = firstNumber + secondNumber / thirdNumber;
        secondFalseSolution = math.getRandomIntInclusive(1, firstNumber);
        
        possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
        return JSON.stringify({ 
            text: text_easy,
            task: task,
            solution: solution,
            possibleSolutions: possibleSolutions
        });
    }
   
    return JSON.stringify({ 
        text: text_medium_and_hard, 
        task: task,
        solution: solution
    });
};

module.exports.divisionOfDifferenceWithOneDigitNumber = function(level) { // TODO
    // (12 - 6) / 2 kako obezbediti da umanjilac bude manji od umanjenika i da je deljiv sa 2
};

module.exports.divisionOfSumOrDifferenceByOneDigitNumber = function(level) { // TODO
    // switch(math.getRandomIntInclusive(1, 2)) {
    //     case 1:
    //         return this.divisionOfSumWithOneDigitNumber(level);
    //     case 2:
    //         return this.divisionOfDifferenceWithOneDigitNumber(level);
    //     default:
    //         {};
    // }
    return this.divisionOfSumWithOneDigitNumber(level);
};


module.exports.divisionOfTwoDigitNumberByOneDigitNumber = function(level) {
    secondNumber = math.getRandomIntInclusive(1, 9);

    if(level == "easy") { 
        firstNumber = math.getRandomIntInclusive(1, 4) * secondNumber;
    } else if (level == "medium" || level == "hard") { 
        firstNumber = math.getRandomIntInclusive(5, 10) * secondNumber;
    } 

    solution = firstNumber / secondNumber;

    task = firstNumber.toString() + " : " + secondNumber.toString() + " = ";

    if(level == "easy") {
        firstFalseSolution = firstNumber - secondNumber;
        secondFalseSolution = firstNumber + secondNumber;
        
        possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
        return JSON.stringify({ 
            text: text_easy,
            task: task,
            solution: solution,
            possibleSolutions: possibleSolutions
        });
    }
   
    return JSON.stringify({ 
        text: text_medium_and_hard, 
        task: task,
        solution: solution
    });
};

module.exports.determiningUnknownDividend = function(level) {
    divisor = math.getRandomIntInclusive(1, 9);
    quotient = math.getRandomIntInclusive(0, 100 / divisor);
    solution = divisor * quotient;

    task = "x : " + divisor.toString() + " = " + quotient.toString() + ", x = ";

    if(level == "easy") {
        firstFalseSolution = quotient / divisor;
        secondFalseSolution = quotient + divisor;
        
        possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
        return JSON.stringify({ 
            text: text_easy,
            task: task,
            solution: solution,
            possibleSolutions: possibleSolutions
        });
    }
       
    return JSON.stringify({ 
            text: text_medium_and_hard, 
            task: task,
            solution: solution
    });
};

module.exports.determiningUnknownDivisor = function(level) {
    solution = math.getRandomIntInclusive(1,9); 
    quotient = math.getRandomIntInclusive(1, 100/solution);
    dividend = solution * quotient;

    task = dividend.toString() + " : x = " + quotient.toString() + ", x = ";
 
    if(level == "easy") {
        firstFalseSolution = dividend - quotient;
        secondFalseSolution = dividend + quotient;
        
        possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
        return JSON.stringify({ 
            text: text_easy,
            task: task,
            solution: solution,
            possibleSolutions: possibleSolutions
        });
    }
       
    return JSON.stringify({ 
            text: text_medium_and_hard, 
            task: task,
            solution: solution
    });
};

module.exports.determiningUnknownFactor = function(level) {
    firstFactor = math.getRandomIntInclusive(1,9);
    solution = math.getRandomIntInclusive(0, 100/firstFactor); // secondFactor
    product = firstFactor * solution;

    if (Math.random() < 0.5) {
        task = firstFactor.toString() + " * x = " + product.toString() + ", x = ";
    } else {
        task = "x * " + firstFactor.toString() + " = " + product.toString() + ", x = ";
    }

    if(level == "easy") {
        firstFalseSolution = firstFactor + product;
        secondFalseSolution = solution + firstFactor;
        
        possibleSolutions = math.shuffle([solution, firstFalseSolution, secondFalseSolution]);
        return JSON.stringify({ 
            text: text_easy,
            task: task,
            solution: solution,
            possibleSolutions: possibleSolutions
        });
    }
       
    return JSON.stringify({ 
            text: text_medium_and_hard, 
            task: task,
            solution: solution
    });
};  