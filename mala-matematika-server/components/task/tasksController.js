const taskGenerator = require('./taskGenerator');
const Topic = require('../topic/topicModel');

async function createTaskByTopicIdAndLevel(topicId, level) {    
    const topic = await Topic.findById(topicId).exec();
    const topicTitle = topic.title;
    
    switch(topicTitle) {
        case "Sabiranje dvocifrenog i jednocifrenog broja":
            return taskGenerator.addingDoubleDigitAndOneDigit(level);
        case "Oduzimanje jednocifrenog broja od dvocifrenog":
            return taskGenerator.substractingOneDigitFromDoubleDigit(level);
        case "Sabiranje dvocifrenih brojeva":
            return taskGenerator.addingDoubleDigits(level);
        case "Oduzimanje dvocifrenih brojeva":
            return taskGenerator.substractingDoubleDigitFromDoubleDigit(level);
        case "Sabiranje broja sa zbirom ili razlikom brojeva":
            return taskGenerator.addingWithSumOrDifference(level);
        case "Nula kao sabirak i umanjilac":
            return taskGenerator.addingAndSubstractingFromZero(level);
        case "Oduzimanje zbira ili razlike od broja":
            return taskGenerator.substractingSumOrDifferenceFromNumber(level);
        case "Oduzimanje broja od zbira ili razlike":
            return taskGenerator.substractingNumberFromSumOrDifference(level);
        case "Odredjivanje nepoznatog sabirka":
            return taskGenerator.determiningUnknownAdditive(level);
        case "Odredjivanje nepoznatog umanjenika":
            return taskGenerator.determiningUnknownMinuend(level);
        case "Odredjivanje nepoznatog umanjioca":
            return taskGenerator.determiningUnknownSubtrahend(level);
        case "Mnozenje broja 2 i brojem 2":
            return taskGenerator.multiplyingNumber2AndBy2(level);
        case "Mnozenje broja 3 i brojem 3":
            return taskGenerator.multiplyingNumber3AndBy3(level);
        case "Mnozenje broja 4 i brojem 4":
            return taskGenerator.multiplyingNumber4AndBy4(level);
        case "Mnozenje broja 5 i brojem 5":
            return taskGenerator.multiplyingNumber5AndBy5(level);
        case "Mnozenje broja 6 i brojem 6":
            return taskGenerator.multiplyingNumber6AndBy6(level);
        case "Mnozenje broja 7 i brojem 7":
            return taskGenerator.multiplyingNumber7AndBy7(level);
        case "Mnozenje broja 8 i brojem 8":
            return taskGenerator.multiplyingNumber8AndBy8(level);
        case "Mnozenje broja 9 i brojem 9":
            return taskGenerator.multiplyingNumber9AndBy9(level);
        case "Mnozenje broja 10 i brojem 10":
            return taskGenerator.multiplyingNumber10AndBy10(level); 
        case "Nula i jedan kao cinioci":
            return taskGenerator.multiplyingWithZeroAndOne(level);
        case "Mnozenje dva broja":
            return taskGenerator.multiplyTwoNumbers(level);
        case "Mnozenje zbira ili razlike jednocifrenim brojem":
            return taskGenerator.multiplyingSumOrDifferenceWithOneDigitNumber(level);     
        case "Mnozenje jednocifrenog i dvocifrenog broja":
            return taskGenerator.multiplyingOneDigitByTwoDigit(level);
        case "Odredjivanje nepoznatog cinioca":
            return taskGenerator.determiningUnknownFactor(level);
        case "Deljenje brojem 2":
            return taskGenerator.divisionBy2(level);
        case "Deljenje brojem 3":
            return taskGenerator.divisionBy3(level);
        case "Deljenje brojem 4":
            return taskGenerator.divisionBy4(level);
        case "Deljenje brojem 5":
            return taskGenerator.divisionBy5(level);
        case "Deljenje brojem 6":
            return taskGenerator.divisionBy6(level);    
        case "Deljenje brojem 7":
            return taskGenerator.divisionBy7(level);
        case "Deljenje brojem 8":
            return taskGenerator.divisionBy8(level);
        case "Deljenje brojem 9":
            return taskGenerator.divisionBy9(level);
        case "Deljenje brojem 10":
            return taskGenerator.divisionBy10(level);
        case "Nula kao deljenik":
            return taskGenerator.division0WithNumber(level);
        case "Jedan kao delilac":
            return taskGenerator.divisionBy1(level);
        case "Deljenje zbira ili razlike jednocifrenim brojem":
            return taskGenerator.divisionOfSumOrDifferenceByOneDigitNumber(level);
        case "Deljenje dvocifrenog broja sa jednocifrenim":
            return taskGenerator.divisionOfTwoDigitNumberByOneDigitNumber(level);
        case "Odredjivanje nepoznatog deljenika":
            return taskGenerator.determiningUnknownDividend(level);
        case "Odredjivanje nepoznatog delioca":
            return taskGenerator.determiningUnknownDivisor(level);
        default:
            {};
    }
}

module.exports.createNTasksByTopicIdAndLevel = async function(req, res, next) {
    const topicId = req.params.topicId
    const level = req.params.level
    const numberOfTasks = req.params.numberOfTasks

    result = []
    for (i = 1; i <= numberOfTasks; i++) {
        try {
            tmp = await createTaskByTopicIdAndLevel(topicId, level);
            result_i = JSON.parse(tmp);
            result_i['id'] = i;
            result.push(result_i);
        } catch(err) {
            next(err);
        }
    }

    try {
        res.status(201).json(result);
    } catch (err) {
        next(err);
    }
};

