const express = require('express');

const router = express.Router();

const controller = require('./tasksController');

// http://localhost:3000/tasks/topicId/level/numberOfTasks
router.get('/:topicId/:level/:numberOfTasks', controller.createNTasksByTopicIdAndLevel);

module.exports = router;
