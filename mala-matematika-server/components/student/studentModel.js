const mongoose = require('mongoose');

const studentSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    firstname: {
        type: String,
        required: true  
    },
    lastname: {
        type: String,
        required: true  
    },
    email: {
        type: String,
        required: true   //TODO ne mogu dvoje dece dodati sa istim emailom roditelja
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true  
    },
    teacherId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Teacher',
        required: true
    },
    classId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Classroom',
        required: true
    }
});

module.exports = mongoose.model('Student', studentSchema);