const express = require('express');

const router = express.Router();

const controller = require('./studentsController');

// http://localhost:3000/students/
router.get('/', controller.getStudents);

// http://localhost:3000/students/
router.post('/', controller.createStudent);

// http://localhost:3000/students/5ec44f5d91ceed284e905598
router.get('/:studentId', controller.getStudentById);

// http://localhost:3000/students/profile/markov
router.post('/profile/:studentUsername', controller.getStudentByUsername);

// http://localhost:3000/students/5ec44f5d91ceed284e905598
router.delete('/:studentId', controller.deleteStudentById);

router.get('/profile/:username', controller.getStudentByUsernameGetmethod);

module.exports = router;
