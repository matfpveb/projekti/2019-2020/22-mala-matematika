const mongoose = require('mongoose');

const Student = require('./studentModel');
const Teacher = require('../teacher/teacherModel');
const Classroom = require('../classroom/classroomModel');

module.exports.getStudents = async function (req, res, next) {
    try {
        const students = await Student.find({}).exec();
        res.status(200).json(students);
    } catch (err) {
        next(err);
    }
};

module.exports.getStudentByUsernameGetmethod = async function (req, res, next) {
    const studentUsername = req.params.username;
    try {
        const student = await Student.findOne({"username": studentUsername}, 'username').exec();

        if (!student) {
            return res.status(401).json({message: 'The student with given username does not exist'});
        }
        res.status(200).json(student);
    } catch (err) {
        next(err);
    }
}

module.exports.createStudent = async function (req, res, next) {
    // Find students teacher by given username
    const teacherUsername = req.body.teacherUsername;
    const teacher = await Teacher.find({"username": teacherUsername}, {_id: 1, classId: 1}).exec();
    
    const teacherId = teacher[0]._id;
    const classId = teacher[0].classId;
    
    const studentObject = {
        _id: new mongoose.Types.ObjectId(),
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email, //TODO prvo roditelj ili ucenik? Kada upariti mejlove?
        username: req.body.username,
        password: req.body.password,
        teacherId: teacherId,
        classId: classId
    };
    const student = new Student(studentObject);
    
    // Adding student to classroom
    const updatedClassroomStudents = await Classroom.updateOne({$push: {studentsIds: student._id}}).exec();

    try {
        const savedStudent = await student.save();
        res.status(201).json({
            message: 'A student is successfully created and added to classroom',
            student: savedStudent,
            addedStudentToClass: updatedClassroomStudents
        });
    } catch (err) {
        next(err);
    }
};

module.exports.getStudentById = async function (req, res, next) {
    const studentId = req.params.studentId;
    try {
        const student = await Student.findById(studentId).exec();

        if (!student) {
            return res.status(404).json({message: 'The student with given id does not exist'});
        }

        res.status(200).json(student);
    } catch (err) {
        next(err);
    }
};

module.exports.getStudentByUsername = async function (req, res, next) {
    const studentUsername = req.body.username;
    const studentPassword = req.body.password;
    try {
        const student = await Student.findOne({"username": studentUsername}).exec();

        if (!student) {
            return res.status(401).json({message: 'The student with given username does not exist'});
        }
        if (!(student.password === req.body.password)) {
            return res.status(401).json({message: 'Incorrect password'})
        }
        res.status(200).json(student);
    } catch (err) {
        next(err);
    }
};

module.exports.deleteStudentById = async function (req, res, next) {
    //TODO ako se obrise student brise se i iz niza child_ids kod parent-a, 
    // i iz niza students_ids iz classroom-a
    const studentId = req.params.studentId;

    try {
        await Student.deleteOne({ _id: studentId }).exec();

        res.status(200).json({ message: 'The student is successfully deleted' });
    } catch (err) {
        next(err);
    }
};
