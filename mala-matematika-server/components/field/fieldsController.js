const mongoose = require('mongoose');

const Field = require('./FieldModel');

module.exports.getFields = async function (req, res, next) {
    try {
        const fields = await Field.find({}).exec();
        res.status(200).json(fields);
    } catch (err) {
        next(err);
    }
};

module.exports.createField = async function (req, res, next) {
    const fieldObject = {
        _id: new mongoose.Types.ObjectId(),
        grade: req.body.grade,
        title: req.body.title
    };
    const field = new Field(fieldObject);

    try {
        const savedField = await field.save();
        res.status(201).json({
            message: 'Field is successfully created',
            field: savedField,
        });
    } catch (err) {
        next(err);
    }
};


module.exports.getFieldById = async function (req, res, next) {
    const fieldId = req.params.fieldId;
    try {
        const field = await Field.findById(fieldId).exec();

        if (!field) {
            return res.status(404).json({message: 'Field with given id does not exist'});
        }

        res.status(200).json(field);
    } catch (err) {
        next(err);
    }
};


module.exports.getFieldsByGrade = async function (req, res, next) {
    const grade = req.params.grade;
    try {
        const fields = await Field.find({"grade": grade}).exec();

        if (!fields) {
            return res.status(404).json({message: 'Fields with given grade does not exist'});
        }

        res.status(200).json(fields);
    } catch (err) {
        next(err);
    }
};

//TODO treba li?
// module.exports.updateByFieldId = async function (req, res, next) {
//   const FieldId = req.params.FieldId;

//   const updateOptions = {};
//   for (let i = 0; i < req.body.length; ++i) {
//     let option = req.body[i];
//     updateOptions[option.nazivPolja] = option.novaVrednost;
//   }

//   try {
//     await Field.updateOne({ _id: FieldId }, { $set: updateOptions }).exec();
//     res.status(200).json({ message: 'The Field is successfully updated' });
//   } catch (err) {
//     next(err);
//   }
// };

module.exports.deleteFieldById = async function (req, res, next) {
    const fieldId = req.params.fieldId;
    try {
        await Field.deleteOne({ _id: fieldId }).exec();
        res.status(200).json({message: 'Field is successfully deleted'});
    } catch (err) {
        next(err);
    }
};
