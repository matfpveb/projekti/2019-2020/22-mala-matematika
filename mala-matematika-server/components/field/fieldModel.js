const mongoose = require('mongoose');

const fieldsSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    grade: {
        type: Number,
        required: true
    },
    title: {
        type: String,
        required: true,
        unique: true
    }
});

const fieldsModel = mongoose.model('Field', fieldsSchema);

module.exports = fieldsModel;