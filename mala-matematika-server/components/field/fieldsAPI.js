const express = require('express');

const router = express.Router();

const controller = require('./fieldsController');

// http://localhost:3000/fields/
router.get('/', controller.getFields);

// http://localhost:3000/fields/
router.post('/', controller.createField);

// http://localhost:3000/fields/5ec44f5d91ceed284e905598
router.get('/:fieldId', controller.getFieldById);

// http://localhost:3000/fields/grade/1
router.get('/grade/:grade', controller.getFieldsByGrade)

// http://localhost:3000/fields/5ec44f5d91ceed284e905598
router.delete('/:fieldId', controller.deleteFieldById);


module.exports = router;