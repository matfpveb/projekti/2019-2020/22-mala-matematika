const mongoose = require('mongoose');

const Topic = require('./topicModel');

module.exports.getTopics = async function (req, res, next) {
    try {
        const topics = await Topic.find({}).exec();
        res.status(200).json(topics);
    } catch (err) {
        next(err);
    }
};

module.exports.createTopic = async function (req, res, next) {
    const topicObject = {
        _id: new mongoose.Types.ObjectId(),
        fieldId: req.body.fieldId,
        title: req.body.title
    };
    const topic = new Topic(topicObject);

    try {
        const savedTopic = await topic.save();
        res.status(201).json({
            message: 'Topic is successfully created',
            topic: savedTopic,
        });
    } catch (err) {
        next(err);
    }
};


module.exports.getTopicById = async function (req, res, next) {
    const topicId = req.params.topicId;
    try {
        const topic = await Topic.findById(topicId).exec();

        if (!topic) {
            return res.status(404).json({message: 'Topic with given id does not exist'});
        }

        res.status(200).json(topic);
    } catch (err) {
        next(err);
    }
};

module.exports.getTopicsByFieldId = async function (req, res, next) {
    const fieldId = req.params.fieldId;
    try {
        const topics = await Topic.find({"fieldId": fieldId}).exec();

        if (!topics) {
            return res.status(404).json({message: 'Topics with given field id does not exist'});
        }

        res.status(200).json(topics);
    } catch (err) {
        next(err);
    }
};

//TODO da li treba?
// module.exports.updateByTopicId = async function (req, res, next) {
//   const TopicId = req.params.TopicId;

//   const updateOptions = {};
//   for (let i = 0; i < req.body.length; ++i) {
//     let option = req.body[i];
//     updateOptions[option.nazivPolja] = option.novaVrednost;
//   }

//   try {
//     await Topic.updateOne({ _id: TopicId }, { $set: updateOptions }).exec();
//     res.status(200).json({ message: 'The Topic is successfully updated' });
//   } catch (err) {
//     next(err);
//   }
// };

module.exports.deleteTopicById = async function (req, res, next) {
    const topicId = req.params.topicId;
    try {
        await Topic.deleteOne({ _id: topicId }).exec();
        res.status(200).json({message: 'Topic is successfully deleted'});
    } catch (err) {
        next(err);
    }
};
