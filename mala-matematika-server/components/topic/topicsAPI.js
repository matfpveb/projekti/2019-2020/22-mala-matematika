const express = require('express');

const router = express.Router();

const controller = require('./topicsController');

// http://localhost:3000/topics/
router.get('/', controller.getTopics);

// http://localhost:3000/topics/
router.post('/', controller.createTopic);

// http://localhost:3000/topics/5ec44f5d91ceed284e905598
router.get('/:topicId', controller.getTopicById);

// http://localhost:3000/topics/field/5ec44f5d91ceed284e905598
router.get('/field/:fieldId', controller.getTopicsByFieldId);

// http://localhost:3000/topics/5ec44f5d91ceed284e905598
router.delete('/:topicId', controller.deleteTopicById);


module.exports = router;
