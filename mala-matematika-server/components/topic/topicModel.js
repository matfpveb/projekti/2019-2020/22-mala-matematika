const mongoose = require('mongoose');

const topicsSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    fieldId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Field',
        required: true 
    },
    title: {
        type: String,
        required: true,
        unique: true
    }
});

const topicsModel = mongoose.model('Topic', topicsSchema);

module.exports = topicsModel;
