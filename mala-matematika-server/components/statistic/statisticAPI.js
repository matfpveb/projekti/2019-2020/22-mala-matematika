const express = require('express');

const router = express.Router();

const controller = require('./statisticController');

// http://localhost:3000/statistics/
router.get('/', controller.getStatistics);

// http://localhost:3000/statistics/
router.post('/', controller.createStatistic);

// http://localhost:3000/statistics/bataVe
router.get('/:studentUsername', controller.getStatisticsByStudentUsername);

// http://localhost:3000/statistics/Sabiranje dvocifrenog i jednocifrenog broja
router.get('/topic/:topicTitle', controller.getStatisticsByTopic);

// http://localhost:3000/statistics/true
router.put('/:isSolved', controller.updateStatisticByParams);

module.exports = router;
