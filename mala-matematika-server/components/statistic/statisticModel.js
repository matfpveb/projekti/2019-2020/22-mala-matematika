const mongoose = require('mongoose');

const statisticSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    studentId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Student',
        required: true 
    },
    topicId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Topic',
        required: true
    },
    difficulty: {
        type: String,
        required: true
    },
    taskText: {
        type: String, 
        required: true
    },
    isSolved: {
        type: Boolean,
        required: true
    }, 
    numberOfIncorrectAttempts: {
        type: Number,
        required: true
    },
    numberOfAttempts: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Statistic', statisticSchema);