const mongoose = require('mongoose');

const Statistic = require('./statisticModel');
const Student = require('../student/studentModel');
const Topic = require('../topic/topicModel');
const { findByIdAndUpdate, findOne } = require('./statisticModel');

module.exports.getStatistics = async function (req, res, next) {
    try {
        const statistics = await Statistic.find({}).exec();
        res.status(200).json(statistics);
    } catch (err) {
        next(err);
    }
};

module.exports.createStatistic = async function (req, res, next) {
    // Find students teacher by given username
    const studentUsername = req.body.studentUsername;
    const student = await Student.find({"username": studentUsername}, {_id: 1}).exec();
    const studentId = student[0]._id;
    
    const topicTitle = req.body.topicTitle;
    const topic = await Topic.find({"title": topicTitle}, {_id: 1}).exec();
    const topicId = topic[0]._id;

    const statisticObject = {
        _id: new mongoose.Types.ObjectId(),
        studentId: studentId,
        topicId: topicId,
        difficulty: req.body.difficulty,
        taskText: req.body.taskText,
        isSolved: false,
        numberOfIncorrectAttempts: 0,
        numberOfAttempts: 0
    };

    const checkForExistanceObject = {
        studentId: studentId,
        topicId: topicId,
        difficulty: req.body.difficulty,
        taskText: req.body.taskText
    };

    
    try {
        const existanceOfStatistic = await Statistic.find(checkForExistanceObject).exec();
        if(existanceOfStatistic.length == 0){
            const statistic = new Statistic(statisticObject);

            const savedStatistic = await statistic.save();
            res.status(201).json({
                message: 'A statistic is successfully created',
                statistic: savedStatistic
            });
        }
        // else {
        //     res.status(409).json({
        //         message: 'Statistic already exist'
        //     });
        // }
    } catch (err) {
        next(err);
    }
};

module.exports.getStatisticsByStudentUsername = async function (req, res, next) {
    const studentUsername = req.params.studentUsername;
    // console.log(req);
    try {
        const studentId = await Student.findOne({"username": studentUsername}).exec();
        if (!studentId) {
            return res.status(401).json({message: 'The student with given username does not exist'});
        }

        const studentStatistic = await Statistic.find({"studentId": studentId}).exec();
        if (!studentStatistic) {
            return res.status(401).json({message: 'The student with given id does not exist'});
        }

        res.status(200).json(studentStatistic);
    } catch (err) {
        next(err);
    }
};

module.exports.getStatisticsByTopic = async function(req, res, next) {
    const topicTitle = req.params.topicTitle;
    try {
        const topicId = await Topic.find({"title": topicTitle}).exec();
        if(!topicId) {
            return res.status(401).json({message: 'The topic with given title does not exist'});
        }

        const topicStatistic = await Statistic.find({"topicId": topicId}).exec();
        if(!topicStatistic) {
            return res.status(401).json({message: 'The topic with given title does not exist'});
        }
        res.status(200).json(topicStatistic);
    } catch (err) {
        next(err);
    }
}


module.exports.updateStatisticByParams = async function (req, res, next) {

    const studentUsername = req.body.studentUsername;
    const topicTitle = req.body.topicTitle;
    const difficulty = req.body.difficulty;
    const text = req.body.taskText;

    try {
        const studentId = await Student.findOne({"username": studentUsername}).exec();
        if (!studentId) {
            return res.status(401).json({message: 'The student with given username does not exist'});
        }

        const topicId = await Topic.findOne({"title": topicTitle}).exec();
        if(!topicId) {
            return res.status(401).json({message: 'The topic with given title does not exist'});
        }
        
        const statisticId = await Statistic.findOne({"studentId": studentId._id, "topicId": topicId._id, "difficulty": difficulty, "taskText": text}).exec();
        if(!statisticId) {
            return res.status(401).json({message: 'The statistic with given params does not exist'});
        }

        var body;
        
        if(req.params.isSolved == "true") {
            body = {
                "isSolved": true,
                "numberOfAttempts": statisticId.numberOfAttempts + 1
            };
        }
        else if(req.params.isSolved == "false") {
            body = {
                "numberOfIncorrectAttempts": statisticId.numberOfIncorrectAttempts + 1,
                "numberOfAttempts": statisticId.numberOfAttempts + 1
            };
        }

        const updateStatistic = await Statistic.findByIdAndUpdate({ _id: statisticId._id }, body).exec();
        if(!updateStatistic){
            return res.status(401).json({message: 'The statistic couldn\'t be updated'});
        }
        res.status(200).json({ message: 'The statistic is successfully updated' });
        } catch (err) {
        next(err);
    }
};
