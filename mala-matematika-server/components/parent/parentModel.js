const mongoose = require('mongoose');

const parentsSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    firstname: {
        type: String,
        required: true  
    },
    lastname: {
        type: String,
        required: true  
    },
    email: {
        type: String,
        required: true,
        unique: true 
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true  
    },
    childIds: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Student',
        required: true
    }]
});

const parentsModel = mongoose.model('Parent', parentsSchema);

module.exports = parentsModel;
