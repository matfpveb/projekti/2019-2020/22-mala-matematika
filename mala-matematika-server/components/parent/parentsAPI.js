const express = require('express');

const router = express.Router();

const controller = require('./parentsController');

// http://localhost:3000/parents/
router.get('/', controller.getParents);

// http://localhost:3000/parents/
router.post('/', controller.createParent);

// http://localhost:3000/parents/5ec44f5d91ceed284e905598
router.get('/:parentId', controller.getParentById);

// http://localhost:3000/parents/5ec44f5d91ceed284e905598
router.delete('/:parentId', controller.deleteParentById);

//TODO da li nam treba?
// router.patch('/:parentId', controller.updateByParentId);

module.exports = router;
