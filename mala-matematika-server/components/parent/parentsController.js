const mongoose = require('mongoose');

const Parent = require('./parentModel');
const Student = require('../student/studentModel');

module.exports.getParents = async function (req, res, next) {
    try {
        const parents = await Parent.find({}).exec();
        res.status(200).json(parents);
    } catch (err) {
        next(err);
    }
};

module.exports.createParent = async function (req, res, next) {
    // Find children ids with email
    const parentEmail = req.body.email;
    const children = await Student.find({"email": parentEmail}, {_id: 1}).exec();
    
    if(children === undefined) {
        childIds = [];
    } else {
        childIds = children.map(x => x._id); //TODO Proveriti ako vise dece ima roditelj da li se lepo napakuje niz id-a dece
    }

    // console.log(child_ids);

    const parentObject = {
        _id: new mongoose.Types.ObjectId(),
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password, 
        childIds: childIds // TODO dohvatiti id dece preko zajednickog mejla
    };
    const parent = new Parent(parentObject);

    try {
        const savedParent = await parent.save();
        res.status(201).json({
            message: 'A parent is successfully created',
            parent: savedParent,
        });
    } catch (err) {
        next(err);
    }
};

module.exports.getParentById = async function (req, res, next) {
    const parentId = req.params.parentId;
    try {
        const parent = await Parent.findById(parentId).exec();

        if (!parent) {
            return res.status(404).json({message: 'The parent with given id does not exist'});
        }

        res.status(200).json(parent);
    } catch (err) {
        next(err);
    }
};

// module.exports.updateByParentId = async function (req, res, next) {
//   const parentId = req.params.parentId;

//   const updateOptions = {};
//   for (let i = 0; i < req.body.length; ++i) {
//     let option = req.body[i];
//     updateOptions[option.nazivPolja] = option.novaVrednost;
//   }

//   try {
//     await Parent.updateOne({ _id: parentId }, { $set: updateOptions }).exec();
//     res.status(200).json({ message: 'The parent is successfully updated' });
//   } catch (err) {
//     next(err);
//   }
// };

module.exports.deleteParentById = async function (req, res, next) {
    const parentId = req.params.parentId;
    try {
        await Parent.deleteOne({ _id: parentId }).exec();

        res.status(200).json({message: 'The parent is successfully deleted'});
    } catch (err) {
        next(err);
    }
};
