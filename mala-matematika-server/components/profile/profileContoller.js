const mongoose = require('mongoose');

const Student = require('../student/studentModel');
const Teacher = require('../teacher/teacherModel');
const Parent = require('../parent/parentModel');

module.exports.getProfiles = async function (req, res, next) {
    try {
        const students = await Student.find({}).exec();
        const teachers = await Teacher.find({}).exec();
        const parents = await Parent.find({}).exec();

        res.status(200).json(students + teachers + parents);
    } catch (err) {
        next(err);
    }
};

module.exports.getProfileByUsername = async function (req, res, next) {
    const profileUsername = req.body.username;
    const profilePassword = req.body.password;
    try {
        profile = await Student.findOne({"username": profileUsername}).exec();

        if (!profile) {
            console.log('The student with given username does not exist')

            profile = await Parent.findOne({"username": profileUsername}).exec();

            if(!profile) {
                console.log('The parent with given username does not exist')
                profile = await Teacher.findOne({"username": profileUsername}).exec();

                if(!profile) {
                    console.log('The teacher with given username does not exist')
                    return res.status(401).json({message: 'The profile with given username does not exist'});
                }
            }
        }

        if (!(profile.password === req.body.password)) {
            return res.status(401).json({message: 'Incorrect password'})
        }
        res.status(200).json(profile);
    } catch (err) {
        next(err);
    }
};