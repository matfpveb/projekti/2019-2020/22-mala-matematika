const express = require('express');

const router = express.Router();

const controller = require('./profileContoller');

// http://localhost:3000/profiles/
router.get('/', controller.getProfiles);

// http://localhost:3000/profiles/markov
router.post('/:profileUsername', controller.getProfileByUsername);

module.exports = router;
