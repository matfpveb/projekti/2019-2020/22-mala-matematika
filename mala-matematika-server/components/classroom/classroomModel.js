const mongoose = require('mongoose');

const classroomSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    grade: {
        type: Number,
        required: true
    },
    teacherId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Teacher',
        required: true
    },
    studentsIds: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Student'
    }]
});

module.exports = mongoose.model('Classroom', classroomSchema);