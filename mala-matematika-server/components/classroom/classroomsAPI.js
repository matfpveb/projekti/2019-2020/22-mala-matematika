const express = require('express');

const router = express.Router();

const controller = require('./classroomsController');

// http://localhost:3000/classrooms/
router.get('/', controller.getClassrooms);

// http://localhost:3000/classrooms/5ec44f5d91ceed284e905597
router.get('/:classroomId', controller.getByClassroomId);

// http://localhost:3000/classrooms/5ec44f5d91ceed284e905597
router.delete('/:classroomId', controller.deleteByClassroomId);

module.exports = router;
