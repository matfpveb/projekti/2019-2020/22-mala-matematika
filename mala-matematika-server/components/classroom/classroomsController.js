const mongoose = require('mongoose');

const Classroom = require('./classroomModel');

module.exports.getClassrooms = async function (req, res, next) {
    try {
        const classrooms = await Classroom.find({}).exec();
        res.status(200).json(classrooms);
    } catch (err) {
        next(err);
    }
};

module.exports.getByClassroomId = async function (req, res, next) {
    const classroomId = req.params.classroomId;
    try {
        const classroom = await Classroom.findById(classroomId).exec();

        if (!classroom) {
            return res.status(404).json({message: 'The classroom with given id does not exist'});
        }
        
        res.status(200).json(classroom);
    } catch (err) {
        next(err);
    }
};

module.exports.deleteByClassroomId = async function (req, res, next) {
    const classroomId = req.params.classroomId;
    try {
        await Classroom.deleteOne({_id: classroomId}).exec();

        res.status(200).json({message: 'The classroom is successfully deleted'});
    } catch (err) {
        next(err);
    }
};
