const mongoose = require('mongoose');

const Teacher = require('./teacherModel');
const Classroom = require('../classroom/classroomModel');

module.exports.getTeachers = async function (req, res, next) {
    try {
        const teachers = await Teacher.find({}).exec();
        res.status(200).json(teachers);
    } catch (err) {
        next(err);
    }
};

module.exports.createTeacher = async function (req, res, next) {
    // Adding teacher
    const teacherObject = {
        _id: new mongoose.Types.ObjectId(),
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
        classId: new mongoose.Types.ObjectId(),
    };
    const teacher = new Teacher(teacherObject);

    // Adding classroom
    const classroomObject = {
        _id: teacher.classId,
        grade: req.body.classGrade,
        teacherId: teacher._id,
        studentsIds: []
    };
    const classroom = new Classroom(classroomObject);

    try {
        const savedTeacher = await teacher.save();
        const savedClassroom = await classroom.save();

        res.status(201).json({
            message: 'A teacher with classroom is successfully created',
            teacher: savedTeacher,
            classroom: savedClassroom
        });
    } catch (err) {
        next(err);
    }
};

module.exports.getTeacherById = async function (req, res, next) {
    const teacherId = req.params.teacherId;
    try {
        const teacher = await Teacher.findById(teacherId).exec();

        if (!teacher) {
            return res.status(404).json({message: 'The teacher with given id does not exist'});
        }

        res.status(200).json(teacher);
    } catch (err) {
        next(err);
    }
};

module.exports.getTeacherByUsername = async function (req, res, next) {
    const teacherUsername = req.body.username;
    const teacherPassword = req.body.password;

    try {
        const teacher = await Teacher.findOne({"username": teacherUsername}).exec();

        if (!teacher) {
            return res.status(401).json({message: 'The teacher with given username does not exist'});
        }
        if (!(teacher.password === teacherPassword)) {
            return res.status(401).json({message: 'Incorrect password'})
        }
        res.status(200).json(teacher);
    } catch (err) {
        next(err);
    }
};

module.exports.getTeacherByUsernameGetmethod = async function (req, res, next) {
    const teacherUsername = req.params.username;
    try {
        const teacher = await Teacher.findOne({"username": teacherUsername}, 'username').exec();

        if (!teacher) {
            return res.status(401).json({message: 'The teacher with given username does not exist'});
        }
        res.status(200).json(teacher);
    } catch (err) {
        next(err);
    }
}

//TODO - ako smo obrisali nastavnika, trebalo bi skloniti u njegovom odeljenju njegov id 
// i postaviti ga na null/prazno/undefined
module.exports.deleteTeacherById = async function (req, res, next) {
    const teacherId = req.params.teacherId;
    try {
        await Teacher.deleteOne({_id: teacherId}).exec();
        res.status(200).json({message: 'The teacher is successfully deleted'});
    } catch (err) {
        next(err);
    }
};
