const express = require('express');

const router = express.Router();

const controller = require('./teachersController');

// http://localhost:3000/teachers/
router.get('/', controller.getTeachers);

// http://localhost:3000/teachers/
router.post('/', controller.createTeacher);

// http://localhost:3000/teachers/5ec44f5d91ceed284e905598
router.get('/:teacherId', controller.getTeacherById);

router.get('/profile/:username', controller.getTeacherByUsernameGetmethod);

// http://localhost:3000/teachers/profile
router.post('/profile', controller.getTeacherByUsername);

// http://localhost:3000/teachers/5ec44f5d91ceed284e905598
router.delete('/:teacherId', controller.deleteTeacherById);

//TODO da li nam treba?
// router.patch('/:teacherId', controller.updateByTeacherId);

module.exports = router;
