use MalaMatematika

db.fields.insert({grade: 2, title: "Brojevi do 100 - Sabiranje i oduzimanje"})
id=db.fields.findOne({title: "Brojevi do 100 - Sabiranje i oduzimanje"})._id

db.topics.insert({fieldId: id, title: "Sabiranje dvocifrenog i jednocifrenog broja"})
db.topics.insert({fieldId: id, title: "Oduzimanje jednocifrenog broja od dvocifrenog"})
db.topics.insert({fieldId: id, title: "Sabiranje dvocifrenih brojeva"})
db.topics.insert({fieldId: id, title: "Oduzimanje dvocifrenih brojeva"})
db.topics.insert({fieldId: id, title: "Sabiranje broja sa zbirom ili razlikom brojeva"})
db.topics.insert({fieldId: id, title: "Nula kao sabirak i umanjilac"})
db.topics.insert({fieldId: id, title: "Oduzimanje zbira ili razlike od broja"})
db.topics.insert({fieldId: id, title: "Oduzimanje broja od zbira ili razlike"})
db.topics.insert({fieldId: id, title: "Odredjivanje nepoznatog sabirka"})
db.topics.insert({fieldId: id, title: "Odredjivanje nepoznatog umanjenika"})
db.topics.insert({fieldId: id, title: "Odredjivanje nepoznatog umanjioca"})

db.fields.insert({grade: 2, title: "Brojevi do 100 - Množenje i deljenje"})
id2=db.fields.findOne({title: "Brojevi do 100 - Množenje i deljenje"})._id

db.topics.insert({fieldId: id2, title: "Proizvod dva broja"})
db.topics.insert({fieldId: id2, title: "Mnozenje broja 2 i brojem 2"})
db.topics.insert({fieldId: id2, title: "Mnozenje broja 10 i brojem 10"})
db.topics.insert({fieldId: id2, title: "Mnozenje broja 5 i brojem 5"})
db.topics.insert({fieldId: id2, title: "Mnozenje broja 4 i brojem 4"})
db.topics.insert({fieldId: id2, title: "Mnozenje broja 3 i brojem 3"})
db.topics.insert({fieldId: id2, title: "Mnozenje broja 6 i brojem 6"})
db.topics.insert({fieldId: id2, title: "Mnozenje broja 7 i brojem 7"})
db.topics.insert({fieldId: id2, title: "Mnozenje broja 8 i brojem 8"})
db.topics.insert({fieldId: id2, title: "Mnozenje broja 9 i brojem 9"})
db.topics.insert({fieldId: id2, title: "Nula i jedan kao cinioci"})
db.topics.insert({fieldId: id2, title: "Nula kao deljenik"})
db.topics.insert({fieldId: id2, title: "Jedan kao delilac"})
db.topics.insert({fieldId: id2, title: "Deljenje brojem 2"})
db.topics.insert({fieldId: id2, title: "Deljenje brojem 3"})
db.topics.insert({fieldId: id2, title: "Deljenje brojem 4"})
db.topics.insert({fieldId: id2, title: "Deljenje brojem 5"})
db.topics.insert({fieldId: id2, title: "Deljenje brojem 6"})
db.topics.insert({fieldId: id2, title: "Deljenje brojem 7"})
db.topics.insert({fieldId: id2, title: "Deljenje brojem 8"})
db.topics.insert({fieldId: id2, title: "Deljenje brojem 9"})
db.topics.insert({fieldId: id2, title: "Deljenje brojem 10"})
db.topics.insert({fieldId: id2, title: "Redosled obavljanja racunskih operacija"})
db.topics.insert({fieldId: id2, title: "Mnozenje zbira i razlike brojem"})
db.topics.insert({fieldId: id2, title: "Mnozenje jednocifrenog i dvocifrenog broja"})
db.topics.insert({fieldId: id2, title: "Deljenje zbira i razlike brojem"})
db.topics.insert({fieldId: id2, title: "Deljenje dvocifrenog broja jednocifrenim"})
db.topics.insert({fieldId: id2, title: "Nepoznati cinilac"})
db.topics.insert({fieldId: id2, title: "Nepoznati deljenik"})
db.topics.insert({fieldId: id2, title: "Nepoznati delilac"})
db.topics.insert({fieldId: id2, title: "Nepoznati broj"})




