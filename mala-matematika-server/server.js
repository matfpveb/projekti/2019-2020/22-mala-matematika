const http = require('http');
const app = require('./app');
var cors = require('cors');

const port = 3000;

const server = http.createServer(app);

server.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});

app.use(cors());