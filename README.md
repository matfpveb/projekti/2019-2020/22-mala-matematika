
# Project 22-Mala-matematika

Mala matematika je aplikacija koja bi đacima prvog i drugog razreda pomogla u vežbanju matematike.
Da bi učenik mogao da pristupi zadacima, neophodno je da se registruje, birajući svog nastavnika, a nakon toga i prijavi u aplikaciju. Zatim, svaki učenik ima mogućnost biranja razreda, oblasti, teme, težine zadataka, nakon čega mu se prikazuju X generisanih zadataka prema traženim kriterijumima.
Svaki pokušaj rešavanja zadatka se evidentira u bazi zbog kasnijeg prikazivanja statistike nastavnicima.

## Tehnički detalji

Za bazu je korišćen MongoDB sistem za upravljanje nerelacionim bazama podataka. Za serversku stranu korišćen
je Node.js sa upotrebom Express.js radnog okvira. Za klijentsku stranu korišćen je Angular9.

## Konfiguracija 

Potrebno je skinuti ceo projekat i ispratiti uputstva README.md fajla u folderu mala-matematika-server .

## Pokretanje
Pozicionirati terminal u direktorijum 22-mala-matematika/MalaMatematika/ i pokrenuti naredbu ```ng serve```, dok u direktorijumu 22-mala-matematika/mala-matematika-server/ pokrenuti naredbu ```nodemon server.js``` ili ```node server.js```. Zatim pokrenuti aplikaciju na http://localhost:4200

## Developers

- [Zorana Gajic, 1091/2019](https://gitlab.com/zokaaagajich)
- [Marko Veljkovic, 1096/2019](https://gitlab.com/bataVeljko)
