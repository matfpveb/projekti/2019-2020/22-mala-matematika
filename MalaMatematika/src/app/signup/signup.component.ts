import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { passwordsMustMatch } from './password.validator';
import { Router } from '@angular/router';

import { StudentService } from '../services/student.service';
import { TeacherService } from '../services/teacher.service';
import { ParentService } from '../services/parent.service';

import { AlertService } from '../services/alert.service';
import { TeacherDatabase } from '../models/teacher.database.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  public signupForm: FormGroup;

  public role: string;
  public teachers: Observable<TeacherDatabase[]>;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private studentService: StudentService,
              private teacherService: TeacherService,
              private parentService: ParentService,
              private alertService: AlertService) {
    this.signupForm = this.formBuilder.group({
      firstname: ['', [Validators.required, Validators.maxLength(15), Validators.minLength(2)]],
      lastname: ['', [Validators.required, Validators.maxLength(15), Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required, Validators.minLength(6)]], /*TODO check that it is not in database*/
      password: ['', [Validators.required, Validators.minLength(8)]], /*TODO password must have 1 digit*/
      confirmPassword: ['', [Validators.required]],
      role: ['', Validators.required],
      teacherUsername: ['', Validators.required],
      classGrade: ['', Validators.required]
    }, {
      validator: passwordsMustMatch('password', 'confirmPassword')
    });
  }

  ngOnInit() {
    this.role = '';
    this.teachers = this.teacherService.getTeachers();
  }

  get f() { return this.signupForm.controls; }

  onRoleChange() {
    const roleSelected = this.f.role.value;
    if (roleSelected === 'student') {
      this.signupForm.get('teacherUsername').setValidators([Validators.required]);
      this.signupForm.get('teacherUsername').updateValueAndValidity();

      this.signupForm.get('classGrade').clearValidators();
      this.signupForm.get('classGrade').updateValueAndValidity();
    } else if (roleSelected === 'teacher') {
      this.signupForm.get('teacherUsername').clearValidators();
      this.signupForm.get('teacherUsername').updateValueAndValidity();

      this.signupForm.get('classGrade').setValidators([Validators.required]);
      this.signupForm.get('classGrade').updateValueAndValidity();
    } else if (roleSelected === 'parent') {
      this.signupForm.get('teacherUsername').clearValidators();
      this.signupForm.get('teacherUsername').updateValueAndValidity();

      this.signupForm.get('classGrade').clearValidators();
      this.signupForm.get('classGrade').updateValueAndValidity();
    }

  }

  public submitSignup(data): void {
    if (!this.signupForm.valid) {
      window.alert('Signup form invalid!');
      return;
    }

    console.log(data);

    if (this.f.role.value === 'student') {
      this.studentService.createStudent(data)
      .subscribe(
          _ => {
              this.alertService.success('Registration successful', true);
              this.router.navigate(['/login']);
          },
          error => {
              this.alertService.error(error);
          });
    } else if (this.f.role.value === 'teacher') {
      this.teacherService.createTeacher(data)
      .subscribe(
          _ => {
              this.alertService.success('Registration successful', true);
              this.router.navigate(['/login']);
          },
          error => {
              this.alertService.error(error);
          });
    } else if (this.f.role.value === 'parent') {
      this.parentService.createParent(data)
      .subscribe(
          _ => {
              this.alertService.success('Registration successful', true);
              this.router.navigate(['/login']);
          },
          error => {
              this.alertService.error(error);
          });
    }

    this.signupForm.reset();
  }
}
