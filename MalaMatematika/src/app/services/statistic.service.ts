import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StatisticDatabase } from '../models/statistic.database.model';

@Injectable({
  providedIn: 'root'
})
export class StatisticService {
  private readonly statisticsUrl = 'http://localhost:3000/statistics/';

  constructor(private http: HttpClient) { }

  createStatistic(formData) {
    const body = {...formData};
    return this.http.post(this.statisticsUrl, formData).
      subscribe(r=>{});
  }

  getStatisticByUsername(username: string) {
    return this.http.get<StatisticDatabase[]>(this.statisticsUrl + username);
  }

  getStatisticByTopicTitle(title: string) {
    return this.http.get<StatisticDatabase[]>(this.statisticsUrl + 'topic/' + title);
  }

  updateStatisticByParams(isCorrect: boolean, formData) {
    const body = {...formData};
    const isCorrectString = isCorrect ? 'true' : 'false'
    return this.http.put(this.statisticsUrl + isCorrectString, body).subscribe( r => { });
  }
}
