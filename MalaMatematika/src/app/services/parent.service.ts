import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ParentDatabase } from '../models/parent.database.model';

@Injectable({
  providedIn: 'root'
})
export class ParentService {
  private readonly parentsUrl = 'http://localhost:3000/parents/';
  constructor(private http: HttpClient) { }

  getParents() {
    return this.http.get<ParentDatabase[]>(this.parentsUrl);
  }

  getParentById(id: string) {
    return this.http.get<ParentDatabase>(this.parentsUrl + id);
  }

  createParent(formData) {
    const body = { ...formData};
    return this.http.post(this.parentsUrl, body);
  }

  deleteParentById(id: string) {
    return this.http.delete(this.parentsUrl + id);
  }
}
