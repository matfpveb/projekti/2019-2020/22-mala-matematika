import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TopicDatabase } from '../models/topic.database.model';

@Injectable({
  providedIn: 'root'
})
export class TopicService {
  private readonly topicsUrl = 'http://localhost:3000/topics/';

  constructor(private http: HttpClient) { }

  getTopics() {
    return this.http.get<TopicDatabase[]>(this.topicsUrl);
  }

  getTopicById(id: string) {
    return this.http.get<TopicDatabase>(this.topicsUrl + id);
  }

  getTopicsByFieldId(fieldId: string) {
    return this.http.get<TopicDatabase[]>(this.topicsUrl + 'field/' + fieldId);
  }

  createTopic(formData) {
    const body = {... formData};
    return this.http.post(this.topicsUrl, body);
  }

  deleteTopicById(id: string) {
    return this.http.delete(this.topicsUrl + id);
  }
}


