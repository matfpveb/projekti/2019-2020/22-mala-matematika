import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { StudentDatabase } from '../models/student.database.model';
import { StudentRequest } from '../models/student.request.model';
import { Student } from '../models/student';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  private readonly studentsUrl = 'http://localhost:3000/students/';
  private readonly studentUsernamesUrl = 'http://localhost:3000/students/profile/';

  constructor(private http: HttpClient) { }

  getStudents() {
    return this.http.get<StudentDatabase[]>(this.studentsUrl);
  }

  getStudentById(id: string) {
    return this.http.get<StudentDatabase>(this.studentsUrl + id);
  }

  getStudentByUsername(username: string) {
    return this.http.get<StudentDatabase>(this.studentUsernamesUrl + username);
  }

  getStudentExistanceByUsername(username: string) {
    return this.http.get<String>(this.studentUsernamesUrl + username);
  }

  createStudent(formData) {
    const body = { ...formData};
    return this.http.post(this.studentsUrl, body);
  }

  // update(user: User) {
  //   return this.http.put('/users/' + user.id, user);
  // }

  deleteStudentById(id: string) {
    return this.http.delete(this.studentsUrl + id);
  }
}
