import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Task } from '../models/task.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private readonly tasksUrl = 'http://localhost:3000/tasks/';

  constructor(private http: HttpClient) { }

  createNTasksByTopicIdAndLevel(topicId: string, level: string, numberOfTasks: number) : Observable<Task[]> {
    return this.http.get<Task[]>(this.tasksUrl + topicId + '/' + level + '/' + numberOfTasks);
  }

  // getTasksByTopicId(topicId: string) {
  //   return this.http.get<Task[]>(this.tasksUrl + 'topic/' + topicId);
  // }
}

