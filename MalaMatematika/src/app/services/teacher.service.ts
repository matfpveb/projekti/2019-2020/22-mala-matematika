import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { TeacherDatabase } from '../models/teacher.database.model';
import { TeacherRequest } from '../models/teacher.request.model';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {
  private readonly teachersUrl = 'http://localhost:3000/teachers/';
  private readonly teachersUsernameUrl = 'http://localhost:3000/teachers/profile/';
  constructor(private http: HttpClient) { }

  getTeachers() {
    return this.http.get<TeacherDatabase[]>(this.teachersUrl);
  }

  getTeacherById(id: string) {
    return this.http.get<TeacherDatabase>(this.teachersUrl + id);
  }

  getTeacherByUsername(username: string) {
    return this.http.post<TeacherDatabase>(this.teachersUsernameUrl, {'username': username});
  }

  getTeacherExistanceByUsername(username: string) {
    return this.http.get<String>(this.teachersUsernameUrl + username);
  }

  createTeacher(formData) {
    const body = { ...formData};
    return this.http.post(this.teachersUrl, body);
  }

  deleteTeacherById(id: string) {
    return this.http.delete(this.teachersUrl + id);
  }
}
