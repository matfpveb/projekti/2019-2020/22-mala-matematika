import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FieldDatabase } from '../models/field.database.model';

@Injectable({
  providedIn: 'root'
})
export class FieldService {
  private readonly fieldsUrl = 'http://localhost:3000/fields/';

  constructor(private http: HttpClient) { }

  getFields() {
    return this.http.get<FieldDatabase[]>(this.fieldsUrl);
  }

  getFieldById(id: string) {
    return this.http.get<FieldDatabase>(this.fieldsUrl + id);
  }

  getFieldsByGrade(grade: number) {
    return this.http.get<FieldDatabase[]>(this.fieldsUrl + 'grade/' + grade);
  }

  createField(formData) {
    const body = {... formData};
    return this.http.post(this.fieldsUrl, body);
  }

  deleteFieldById(id: string) {
    return this.http.delete(this.fieldsUrl + id);
  }
}
