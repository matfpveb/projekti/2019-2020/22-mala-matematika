export interface TeacherDatabase {
  _id: string;
  firstname: string;
  lastname: string;
  email: string;
  username: string;
  password: string;
  classId: string;
}
