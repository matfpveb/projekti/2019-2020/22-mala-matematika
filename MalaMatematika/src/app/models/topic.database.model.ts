export interface TopicDatabase {
  _id: string;
  fieldId: string;
  title: string;
}
