export interface ParentRequest {
  firstname: string;
  lastname: string;
  email: string;
  username: string;
  password: string;
}
