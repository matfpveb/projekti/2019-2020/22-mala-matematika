export interface StudentDatabase {
  _id: string;
  firstname: string;
  lastname: string;
  email: string;
  username: string;
  password: string;
  teacherId: string;
  classId: string;
}
