export interface StudentRequest {
  firstname: string;
  lastname: string;
  email: string;
  username: string;
  password: string;
  teacherUsername: string;
}
