export interface StatisticDatabase {
    _id: string;
    studentId: string;
    topicId: string;
    difficulty: string;
    taskText: string;
    isSolved: boolean;
    numberOfIncorrectAttempts: number;
    numberOfAttempts: number;
  }