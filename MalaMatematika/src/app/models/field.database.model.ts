export interface FieldDatabase {
  _id: string;
  grade: number;
  title: string;
}
