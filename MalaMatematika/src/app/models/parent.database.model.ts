export interface ParentDatabase {
  _id: string;
  firstname: string;
  lastname: string;
  email: string;
  username: string;
  password: string;
  childIds: string[];
}
