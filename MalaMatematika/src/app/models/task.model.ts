export interface Task {
  id: number;
  text: string;
  task: string;
  solution: number;
  possibleSolutions?: Array<number>;
}
