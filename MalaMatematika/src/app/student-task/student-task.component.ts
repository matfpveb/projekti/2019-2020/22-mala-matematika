import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { FieldDatabase } from '../models/field.database.model';
import { FieldService } from '../services/field.service';
import { TopicDatabase } from '../models/topic.database.model';
import { TopicService } from '../services/topic.service';
import { TaskService } from '../services/task.service';
import { StudentService } from '../services/student.service';
import { Task } from '../models/task.model';
import { StatisticService } from '../services/statistic.service';
import { StatisticDatabase } from '../models/statistic.database.model';
import { StudentDatabase } from '../models/student.database.model';
import { TeacherService } from '../services/teacher.service';
import { TeacherDatabase } from '../models/teacher.database.model';

@Component({
  selector: 'app-student-task',
  templateUrl: './student-task.component.html',
  styleUrls: ['./student-task.component.css']
})
export class StudentTaskComponent implements OnInit {

  public selectedGrade;
  public selectTaskForm: FormGroup;

  public fields: Observable<FieldDatabase[]>;
  public topics: Observable<TopicDatabase[]>;
  public tasks: Observable<Task[]>;
  public taskArray: Task[];
  public studentStatistic: Observable<StatisticDatabase[]>;
  public topicStatistic: Observable<StatisticDatabase[]>;
  public topicTitles: Set<String>;
  public studentUsernames: Set<String>;
  public studentUsernamesFromDatabase: Observable<StudentDatabase[]>;
  public fieldTitlesFromDatabase: Observable<FieldDatabase[]>;
  public topicTitlesFromDatabase: Observable<TopicDatabase[]>;
  public levelSelected: String;

  public chosenTopic;
  public chosenTask: Task;
  public chosenLevel;
  public currentUser;
  public chosenStatistic;
  public chosenUsername;
  public chosenTopicTitle;
  public chosenFieldID;
  public isFieldChosen: Boolean = false;
  public showTask: Boolean = false;

  public imageSource: string;

  public easyLevelStudentsSolution = null;
  public medhardStudentsSolution: string;

  public studentStatisticByTopics: Map<string, Map<string, [number, number, number]>>;
  public topicStatisticByUsernames: Map<string, Map<string, [number, number]>>;

  public isTeacher: boolean;

  constructor(private formBuilder: FormBuilder,
              private fieldsService: FieldService,
              private topicsService: TopicService,
              private tasksService: TaskService,
              private statisticService: StatisticService,
              private studentService: StudentService,
              private teacherService: TeacherService
    ) {
    this.selectTaskForm = this.formBuilder.group({
      grade: ['', [Validators.required]],
      field: ['', [Validators.required]],
      topic: ['', [Validators.required]],
      level: ['', [Validators.required]],
      task: ['', [Validators.required]],
      statistic: ['', []],
      statisticU: ['', []],
      statisticT: ['', []],
      statisticF: ['', []]
    });
  }

  ngOnInit() {
    const currentUrl = window.location.href;
    this.currentUser = currentUrl.substring(currentUrl.lastIndexOf('/') + 1, currentUrl.length);
    this.isCurrentUserTeacher();
  }

  isCurrentUserTeacher() {
    let returnValue = false;
    this.teacherService.getTeacherExistanceByUsername(this.currentUser).subscribe(
      s => {
        this.isTeacher = true;
      },
      error => {
        this.isTeacher = false;
      }
    );
  }

  get f() { return this.selectTaskForm.controls; }

  onChangeGrade() {
    const gradeSelected = this.f.grade.value;
    this.fields = this.fieldsService.getFieldsByGrade(gradeSelected);
    this.showTask = false;
  }

  onChangeField() {
    const fieldSelected = this.f.field.value;
    this.topics = this.topicsService.getTopicsByFieldId(fieldSelected);
    this.showTask = false;
  }

  onChangeTopic($event) {
    this.chosenTopic = $event.target.options[$event.target.options.selectedIndex].text;
    
    const topicSelected = this.f.topic.value;
    this.showTask = false;
    if(this.levelSelected) {
      this.onChangeLevel();
    }
  }

  onChangeLevel() {
    const topicSelected = this.f.topic.value;
    this.levelSelected = this.f.level.value;
    const numberOfTasks = 10;
    
    (document.getElementById('CheckImage') as HTMLImageElement).src = '';

    this.tasks = this.tasksService.createNTasksByTopicIdAndLevel(topicSelected, this.levelSelected.toString(), numberOfTasks);

    this.tasksService.createNTasksByTopicIdAndLevel(topicSelected, this.levelSelected.toString(), numberOfTasks).subscribe(tasks => {
      this.taskArray = tasks;
    });
    this.showTask = false;

  }

  onChangeTask() {
    const taskIndexSelected = this.f.task.value;
    this.chosenLevel = this.f.level.value;

    (document.getElementById('CheckImage') as HTMLImageElement).src = '';

    this.chosenTask = this.taskArray[taskIndexSelected];
    this.showTask = true;
  }

  onClickCheckTask() {
    this.chosenLevel = this.f.level.value;

    this.createStatistic();
    const bodyStatistic = {
      studentUsername: this.currentUser,
      topicTitle: this.chosenTopic,
      difficulty: this.chosenLevel,
      taskText: this.chosenTask.task
    };

    let studentsSolution;
    if (this.chosenLevel === 'easy') {
      studentsSolution = parseInt(this.easyLevelStudentsSolution, 10);
    } else if (this.chosenLevel === 'medium' || this.chosenLevel === 'hard') {
      studentsSolution =  parseInt(this.medhardStudentsSolution, 10);
    }

    // If possible solution is chosen by student then check the correctness
    if (studentsSolution !== null) {
      if (this.chosenTask.solution === studentsSolution) {
        (document.getElementById('CheckImage') as HTMLImageElement).src = '../../assets/img/happy_smiley.png';
        // console.log(bodyStatistic);
        this.statisticService.updateStatisticByParams(true, bodyStatistic);
      } else {
        (document.getElementById('CheckImage') as HTMLImageElement).src = '../../assets/img/sad_smiley.png';
        // console.log(bodyStatistic);
        this.statisticService.updateStatisticByParams(false, bodyStatistic);
      }
    }

    this.easyLevelStudentsSolution = null;
    this.medhardStudentsSolution = null;
  }

  onClickGiveUp() {
    this.showTask = false;
  }

  // Statistic part - only for professors

  onChangeStatistic() {
    this.chosenStatistic = this.f.statistic.value;

    if (this.chosenStatistic === 'student') {
      this.studentUsernamesFromDatabase = this.studentService.getStudents();
    } else if (this.chosenStatistic === 'field') {
      this.fieldTitlesFromDatabase = this.fieldsService.getFields();
    } else if (this.chosenStatistic === 'classroom') {

    }
  }

  onSelectU() {
    this.studentStatisticByTopics = new Map<string, Map<string, [number, number, number]>>();
    this.chosenUsername = this.f.statisticU.value;

    this.studentStatistic = this.statisticService.getStatisticByUsername(this.chosenUsername);
    this.topicTitles = new Set<string>();
    const subscription = this.studentStatistic.subscribe(ss => {
      ss.forEach(s => {
        console.log(s.topicId);
        const tmp = this.topicsService.getTopicById(s.topicId);
        tmp.subscribe(t => {
          if (this.studentStatisticByTopics.has(t.title)) {
            const difficultyLength = s.difficulty.length

            var localDifficulty = s.difficulty == 'easy' ? '1'+s.difficulty : s.difficulty == 'medium' ? '2'+s.difficulty : '3'+s.difficulty;
            console.log(t.title + localDifficulty);

            const numOfAttempts = this.studentStatisticByTopics.get(t.title).get(localDifficulty)[0];
            const numOfCorrect = this.studentStatisticByTopics.get(t.title).get(localDifficulty)[1];
            const numOfIncorrect = this.studentStatisticByTopics.get(t.title).get(localDifficulty)[2];

            const isSolvedNumber = s.isSolved ? 1 : 0;
            this.studentStatisticByTopics.get(t.title)
                .set(localDifficulty, [numOfAttempts + s.numberOfAttempts, numOfCorrect + isSolvedNumber, numOfIncorrect + s.numberOfIncorrectAttempts]);
          } else {
            //Added 1, 2 and 3 as prefix because of the alphabetical sorting on front
            this.studentStatisticByTopics.set(t.title, new Map([['1easy', [0, 0, 0]], ['2medium', [0, 0, 0]], ['3hard', [0, 0, 0]]]));

            const difficultyLength = s.difficulty.length

            var localDifficulty = s.difficulty == 'easy' ? '1'+s.difficulty : s.difficulty == 'medium' ? '2'+s.difficulty : '3'+s.difficulty;

            const numOfAttempts = this.studentStatisticByTopics.get(t.title).get(localDifficulty)[0];
            const numOfCorrect = this.studentStatisticByTopics.get(t.title).get(localDifficulty)[1];
            const numOfIncorrect = this.studentStatisticByTopics.get(t.title).get(localDifficulty)[2];

            const isSolvedNumber = s.isSolved ? 1 : 0;
            this.studentStatisticByTopics.get(t.title)
                .set(localDifficulty, [numOfAttempts + s.numberOfAttempts, numOfCorrect + isSolvedNumber, numOfIncorrect + s.numberOfIncorrectAttempts]);
          }
        });
      });
    },
    undefined,
    () => {
      setTimeout(() => {
        for (let [k, v] of this.studentStatisticByTopics.entries()) {
          console.log(k);
          for (let [k2, v2] of v.entries()) {
            console.log(k2);
            console.log(v2[0]);
            console.log(v2[1]);
            console.log(v2[2]);
          }
        }
      }, 1000);
    });
  }

  onSelectF() {
    this.isFieldChosen = true;
    this.chosenFieldID = this.f.statisticF.value;
    this.topicTitlesFromDatabase = this.topicsService.getTopicsByFieldId(this.chosenFieldID);
  }

  onSelectT() {
    this.topicStatisticByUsernames = new Map<string, Map<string, [number, number]>>();
    this.chosenTopicTitle = this.f.statisticT.value;
    console.log(this.chosenTopicTitle);

    this.topicStatistic = this.statisticService.getStatisticByTopicTitle(this.chosenTopicTitle);
    this.studentUsernames = new Set<string>();
    this.topicStatistic.subscribe(ss => {
      ss.forEach(s => {
        const tmp = this.studentService.getStudentById(s.studentId);
        tmp.subscribe(stud => {
          if (this.topicStatisticByUsernames.has(stud.username)) {
            const numOfCorrect = this.topicStatisticByUsernames.get(stud.username).get(s.difficulty)[0];
            const numOfIncorrect = this.topicStatisticByUsernames.get(stud.username).get(s.difficulty)[1];
            const isSolvedNumber = s.isSolved ? 1 : 0;
            this.topicStatisticByUsernames.get(stud.username)
              .set(s.difficulty, [numOfCorrect + isSolvedNumber, numOfIncorrect + s.numberOfIncorrectAttempts]);
          } else {
            this.topicStatisticByUsernames.set(stud.username, new Map([['easy', [0, 0]], ['medium', [0, 0]], ['hard', [0, 0]]]));
          }
        });
      });
    },
    undefined,
    () => {
      setTimeout(() => {
        for (let [k, v] of this.topicStatisticByUsernames.entries()) {
          console.log(k);
          for (let [k2, v2] of v.entries()) {
            console.log(k2);
            console.log(v2[0]);
            console.log(v2[1]);
          }
        }
      }, 1000);
    });
  }

  createStatistic() {
    const bodyStatistic = {
      studentUsername: this.currentUser,
      topicTitle: this.chosenTopic,
      difficulty: this.chosenLevel,
      taskText: this.chosenTask.task
    };
    const checkThis = this.statisticService.createStatistic(bodyStatistic);
  }

}
