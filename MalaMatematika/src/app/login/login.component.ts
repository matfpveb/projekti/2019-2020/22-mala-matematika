import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StudentService } from '../services/student.service';
import { Student } from '../models/student';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
// import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public student: Student;
  returnUrl: string;
  public isError: boolean;
  public role: string;

  constructor(private formBuilder: FormBuilder,
              private studentService: StudentService,
              private authenticationService: AuthenticationService,
              private router: Router) {
    this.loginForm = this.formBuilder.group({
      // TODO check existance of username in database, check if username and password match
      username: ['', [Validators.required, Validators.minLength(5)]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
    this.isError = false;
  }

  get f() { return this.loginForm.controls; }

  ngOnInit() {
    // reset login status
    this.authenticationService.logout();
  }

  submitLogin(data) {
    console.log(data.username);
    console.log(data.password);
    this.authenticationService.login(data)
        .subscribe(
            dat => {
                this.returnUrl = '/profiles/' + this.f.username.value;
                this.router.navigate([this.returnUrl]);
            },
            error => {
                console.log('2 ' + error.status);
                this.isError = true;
                console.log('error is ', error);
                setTimeout(() => {
                  this.isError = false;
                }, 3000);
            });
  }
}
